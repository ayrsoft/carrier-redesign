<style>
    .page-lists {
        display: flex;
        flex-direction: column;
    }
    .page-lists a {
        color: #23419A;
        font-size: 16px;
        line-height: 130%;
        letter-spacing: 0.02em;
    }
    .page-lists a:hover {
        color: #00A9E8;
    }
</style>

<div class="page-lists">
    <h1>Site Map</h1>
    <!-- <a href="/homepage.php">Homepage</a> -->
    <a href="/product-listing.php">Product Listing</a>
    <a href="/high-wall.php">High Wall</a>
    <div>
        <a href="/search-result.php">Search Result</a> Please see the "No result found design" comment for the empty state.
    </div>
    <a href="/product-detail.php">Product Detail</a>
    <a href="/my-account.php">My Account</a>
</div>