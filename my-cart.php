<?php include 'src/includes/header-cart-breadcrumbs.php';?>

<section class="product-listing my-cart">
    <div class="o-container">
        <div class="all-product">
            <h2>My Cart</h2>
            <h6>1 item</h6>

            <div class="table">
                <div class="layout-inline row full-width my-cart__item">
                    <a href="javascript:void(0)" class="close remove" ><span><img src="src/images/close.png" alt="close"></span></a>
                    <div class="col col-pro layout-inline my-cart__item--desc">
                        <img class="my-cart__item--img" src="src/images/iCool Green Remote SD 0.5 HP.png" alt="iCool Green Remote SD 0.5 HP">
                        <h6>iCool Green Remote SD 0.5 HP<span>WCARH010EE</span></h6>                        
                    </div>
                    <div class="col col-price col-numeric text-center my-cart__item--amt">
                        <p>₱ 15,800</p>
                    </div>
                    <div class="col col-qty layout-inline my-cart__item--qty">
                        <a href="#" class="qty qty-minus"><img src="src/images/minus.png" class="img-fluid" alt="minus"></a>
                        <input type="numeric" value="3">
                        <a href="#" class="qty qty-plus"><img src="src/images/add.png" class="img-fluid" alt="add"></a>
                    </div>
                </div>
            </div>

            <div class="row equal full-width col-2">
                <div class="column">&nbsp;</div>
                <div class="column">
                    
                    <div class="subtotal cf">
                        <ul>
                            <li class="totalRow"><span class="label">Subtotal</span><span class="value">₱ 15,800</span></li>
                            <li class="totalRow"><span class="label">Coupon</span><span class="value">-</span></li>
                        </ul>
                        <div class="shipping">
                            <p class="totalRow"><span class="label">Shipping Options</span></p>
                            <p>Calculated at next step. Read our <a href="javascript:void(0)">Shipping Policy</a></p>
                        </div>
                        <ul>
                            <li class="totalRow final"><span class="label total">Total</span><span class="value">₱ 15,300</span></li>
                            <!-- <li class="totalRow"><span class="label">Tax</span><span class="value">$4.00</span></li> -->
                            <!-- <li class="totalRow"><a href="#" class="btn continue">Checkout</a></li> -->
                        </ul>
                    </div>

                    <div class="row my-cart__cta-button">
                        <a href="javascript:void(0)" class="btn btn__blue--inverse">Continue Shopping</a>
                        <a href="javascript:void(0)" class="btn btn__blue">Check Out</a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>


<div id="modal-container">
  <div class="modal-background">
   

    <!-- Modal -->
    <div class="modal right fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                

                <div class="modal-body">
                  <div class="row full-width hide" data-step="1" data-title="Cart items">
                    <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">My Cart (1 item)</h4>
                        </div>           
                        <div class="modal__cart-item">
                            <div class="modal__cart-item--del text-right">
                                <a href="javascript:void(0)" class="remove"><img src="src/images/trash.png" alt="trash"></a>
                            </div>
                            <div class="row col-3">
                                <div class="column left modal__cart-item--img">
                                    <img src="src/images/cart-iCoolGreenRemote.png" alt="iCoolGreenRemote">
                                </div>
                                <div class="column middle modal__cart-item--desc">
                                    <span>iCool Green Remote SD 0.5 HP x <span class="modal__cart-item--qty">1</span></span>
                                </div>
                                <div class="column right modal__cart-item--amt text-right">
                                    <span>₱ 15,800</span>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__subtotal">
                            <div class="row full-width equal col-2">
                                <div class="column text-left modal__subtotal--title">SUBTOTAL</div>
                                <div class="column text-right modal__subtotal--price">₱ 15,800</div>
                            </div> 
                        </div>

                        <div class="modal-footer modal__btns">
                          <!-- <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button> -->
                          <!-- <button type="button" class="btn btn-warning js-btn-step btn__blue--inverse" data-orientation="previous">View Cart</button> -->
                          <a href="my-cart.php" class="btn btn__blue--inverse">View Cart</a>
                          <button type="button" class="btn btn-success js-btn-step btn__blue" data-orientation="next">Check Out</button>
                        </div>

                    </div>
                  </div>
                  <div class="row full-width hide" data-step="2" data-title="Sold by">
                    <!-- <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Sold by</h4>
                        </div>           
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/rob-appliances.png" alt="Robinsons Appliances"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 15,300</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="javascript:void(0)" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/mega-saver.png" alt="Mega Saver"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 21,500</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="javascript:void(0)" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        
                    </div> -->
                  </div>
                  <div class="row full-width hide" data-step="3" data-title="Sold by">
                    <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Sold by</h4>
                        </div>           
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/rob-appliances.png" alt="Robinsons Appliances"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 15,300</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="my-cart.php" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/mega-saver.png" alt="Mega Saver"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 21,500</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="my-cart.php" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        
                    </div>
                  </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    </div>
  </div>
</div>

<?php include 'src/includes/carrier-services.php';?>
<?php include 'src/includes/footer.php';?>


<script>
$('#myModal1').modalSteps();

// var span = document.getElementsByClassName("close")[0];

// $('.button').click(function(){
//   var buttonId = $(this).attr('id');
//   $('#modal-container').removeAttr('class').addClass(buttonId);
//   $('body').addClass('modal-active');
// })

// $('#modal-container').click(function(){
//   $(this).addClass('out');
//   $('body').removeClass('modal-active');
// });

$('#myModal1').modalSteps({
  btnCancelHtml: "Cancel",
  btnPreviousHtml: "View Cart",
  btnNextHtml: "Check Out",
  btnLastStepHtml: "Complete",
  disableNextButton: false
  // completeCallback: function() {},
  // callbacks: {},
  // getTitleAndStep: function() {}
});

// HTML CSS JSResult Skip Results Iframe EDIT ON
$('.visibility-cart').on('click',function(){
       
  var $btn =  $(this);
  var $cart = $('.cart');
  console.log($btn);
  
  if ($btn.hasClass('is-open')) {
     $btn.removeClass('is-open');
     $btn.text('O')
     $cart.removeClass('is-open');     
     $cart.addClass('is-closed');
     $btn.addClass('is-closed');
  } else {
     $btn.addClass('is-open');
     $btn.text('X')
     $cart.addClass('is-open');     
     $cart.removeClass('is-closed');
     $btn.removeClass('is-closed');
  }

                  
});

  // SHOPPING CART PLUS OR MINUS
  $('a.qty-minus').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());
    
    if (value > 1) {
      value = value - 1;
    } else {
      value = 0;
    }
    
    $input.val(value);
        
  });

  $('a.qty-plus').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest('div').find('input');
    var value = parseInt($input.val());

    if (value < 100) {
    value = value + 1;
    } else {
      value =100;
    }

    $input.val(value);
  });

// RESTRICT INPUTS TO NUMBERS ONLY WITH A MIN OF 0 AND A MAX 100
$('input').on('blur', function(){

  var input = $(this);
  var value = parseInt($(this).val());

    if (value < 0 || isNaN(value)) {
      input.val(0);
    } else if
      (value > 100) {
      input.val(100);
    }
});

// Remove Items From Cart
$('a.remove').click(function(){
  event.preventDefault();
  $( this ).parent().parent().parent().hide( 400 );
 
})

// Just for testing, show all items
  $('a.btn.continue').click(function(){
    $('li.items').show(400);
  })

  

</script>