<?php include 'src/includes/header.php';?>

<section class="product-listing">
    <div class="o-container">
        <form action="" class="classic-form">
            <div class="product-search">
                <h2>All Products</h2>
                <div class="form-holder">
                    <div class="form-group with-search">
                        <input type="text" placeholder="Search">
                    </div>
                </div>
            </div>
            <div class="product-holder">
                <div class="product-filter" data-show="filter">
                    <div class="product-filter__heading">
                        <h5>Filters</h5>
                        <button type="button" class="o-button-default clear-filter" data-action="clear-filter"> 
                            Clear
                        </button>
                        <button type="button" class="modal-close close-filter-mobile" data-modal-close="filter">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18 6L6 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M6 6L18 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </button>
                    </div>
                    <div class="product-filter__horsepower">
                        <span>horsepower</span>
                        <div class="select-container" data-dropdown="hp">
                            <div class="select-container__option--selected" data-dropdown-value="hp">
                                0.5 HP (up to 10sqm)
                            </div>
                            <ul>
                                <li class="c-custom-select__option" data-drop-value="default">0.5 HP (up to 10sqm)</li>
                                <li class="c-custom-select__option">0.75 HP (up to 13 sqm)</li>
                                <li class="c-custom-select__option">1 HP (up to 17 sqm)</li>
                                <li class="c-custom-select__option">1.5 HP (up to 23 sqm)</li>
                                <li class="c-custom-select__option"> 2.5 HP (up to 41 sqm)</li>
                            </ul>
                            <input class="js-custom-selector__value" type="hidden" value />
                        </div>
                    </div>
                    <div class="product-filter__space">
                        <span>Space</span>
                        <ul class="checklist-group__list">
                            <li>
                                <label class="container-checkbox">
                                    Bedroom
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Living Room
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Office
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Commercial spaces
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="product-filter__area range-area">
                        <span>room size (area)</span>
                        <p>From <span data-slide="0">10 sqm</span> – <span data-slide="1">14 sqm</span></p>
                        <div class="custom-range">
                            <div id="slider"></div>
                        </div>
                        <div class="range-value">
                            <span>&lt;10 sqm</span>
                            <span>45 sqm</span>
                        </div>
                    </div>
                    <div class="product-fiflter__exposure">
                        <span>LIGHT EXPOSURE: Under direct sunlight</span>
                        <ul class="checklist-group__list">
                            <li>
                                <label class="container-checkbox">
                                    Yes
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    No
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="product-filter__occupants">
                        <span>OCCUPANTS IN THE ROOM</span>
                        <div class="select-container" data-dropdown="oc">
                            <div class="select-container__option--selected" data-dropdown-value="oc">
                                1 person
                            </div>
                            <ul>
                                <li class="c-custom-select__option" data-drop-value="default">1 person</li>
                                <li class="c-custom-select__option">2 person</li>
                                <li class="c-custom-select__option">3 person</li>
                                <li class="c-custom-select__option">4 person</li>
                                <li class="c-custom-select__option">5 person</li>
                            </ul>
                            <input class="js-custom-selector__value" type="hidden" value />
                        </div>
                    </div>
                    <div class="product-filter__concern">
                        <span>concern</span>
                        <ul class="checklist-group__list">
                            <li>
                                <label class="container-checkbox">
                                    Energy Efficicency
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Air Quality
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Power
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    Noise reduction
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                    <div class="product-filter__price range-area">
                        <span>price</span>
                        <p><span>₱ 15,000</span> – <span data-slide-fixed="0">₱ 25,000</span></p>
                        <div class="custom-range">
                            <div id="sliderOnePoint"></div>
                        </div>
                    </div>
                    <div class="product-mobile-buttons">
                        <button class="o-button-full">Apply</button>
                        <button type="button" class="o-button-default clear-filter" data-action="clear-filter"> 
                            Clear
                        </button>
                    </div>
                </div>
                <div class="product-sortby">
                    <div class="product-sortby__filter">
                        <div class="filter-button">
                            <button type="button" data-trigger-show="filter">Filter</button>
                        </div>
                        <div class="sortby-filter">
                            <label for="">Sort By: </label>
                            <div class="select-container" data-dropdown="sb">
                                <div class="select-container__option--selected" data-dropdown-value="sb">
                                    Price: Low to High
                                </div>
                                <ul>
                                    <li>Price: Low to low</li>
                                    <li>Price: Low to medium</li>
                                    <li>Price: Low to High</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="product-sortby__products">
                        <div class="cards">
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/product-2.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">iCool Green Remote Top Discharge</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱35,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/alpha-inverter.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Alpha Inverter</div>
                                        <div class="rate">
                                            <div class="rate-stars"></div>
                                            <div class="rate-counts">No reviews yet</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/crystal-inverter.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Crystal Inverter</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/XPower-gold-2-inverter.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">XPower Gold 2 Inverter</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/optima-2.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Optima 2</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/iCool-green.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">iCool Green</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱15,800.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/optima.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Optima</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/klarwind-duo.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Klarwind Duo</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-content__header">
                                        <img src="src/images/klarwind-slimpac-inverter.png" alt="product">
                                    </div>
                                    <div class="card-content__body">
                                        <span class="subtitle">Non-Inverter</span>
                                        <div class="title">Klarwind Slimpac Inverter</div>
                                        <div class="rate">
                                            <div class="rate-stars">
                                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                                </div>
                                            </div>
                                            <div class="rate-counts">(16)</div>
                                        </div>
                                        <span class="price">₱25,000.00</span>
                                    </div>
                                    <div class="card-content__footer">
                                        <button class="o-button-full">Compare</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <li class="no-previous">
                                    <a href="javascript:void(0)">
                                        <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 9L5 5L1 1" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </a>
                                </li>
                                <li class="active"><a href="javascript:void(0)">1</a></li>
                                <li><a href="javascript:void(0)">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li class="more"><a href="javascript:void(0)">...</a></li>
                                <li><a href="javascript:void(0)">12</a></li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1 9L5 5L1 1" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<?php include 'src/includes/footer.php';?>