<?php include 'src/includes/header-cart.php';?>

<section class="product-listing">
    <div class="o-container">
        <div class="all-product">
            <h2>All Products</h2>
            <div class="all-product__category">
                <div class="category-holder">
                    <div class="category-holder__content">
                        <img src="src/images/Carrier-Air-Purifier.svg" alt="air-purifier">
                        <p>Air Conditioner</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div id="modal-container">
  <div class="modal-background">
   

    <!-- Modal -->
    <div class="modal right fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                

                <div class="modal-body">
                  <div class="row full-width hide" data-step="1" data-title="Cart items">
                    <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">My Cart (1 item)</h4>
                        </div>           
                        <div class="modal__cart-item">
                            <div class="modal__cart-item--del text-right">
                                <a href="javascript:void(0)" class="remove"><img src="src/images/trash.png" alt="trash"></a>
                            </div>
                            <div class="row col-3">
                                <div class="column left modal__cart-item--img">
                                    <img src="src/images/cart-iCoolGreenRemote.png" alt="iCoolGreenRemote">
                                </div>
                                <div class="column middle modal__cart-item--desc">
                                    <span>iCool Green Remote SD 0.5 HP x <span class="modal__cart-item--qty">1</span></span>
                                </div>
                                <div class="column right modal__cart-item--amt text-right">
                                    <span>₱ 15,800</span>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__subtotal">
                            <div class="row full-width equal col-2">
                                <div class="column text-left modal__subtotal--title">SUBTOTAL</div>
                                <div class="column text-right modal__subtotal--price">₱ 15,800</div>
                            </div> 
                        </div>

                        <div class="modal-footer modal__btns">
                          <!-- <button type="button" class="btn btn-default js-btn-step pull-left" data-orientation="cancel" data-dismiss="modal"></button> -->
                          <!-- <button type="button" class="btn btn-warning js-btn-step btn__blue--inverse" data-orientation="previous">View Cart</button> -->
                          <a href="my-cart.php" class="btn btn__blue--inverse">View Cart</a>
                          <button type="button" class="btn btn-success js-btn-step btn__blue" data-orientation="next">Check Out</button>
                        </div>

                    </div>
                  </div>
                  <div class="row full-width hide" data-step="2" data-title="Sold by">
                    <!-- <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Sold by</h4>
                        </div>           
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/rob-appliances.png" alt="Robinsons Appliances"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 15,300</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="javascript:void(0)" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/mega-saver.png" alt="Mega Saver"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 21,500</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="javascript:void(0)" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        
                    </div> -->
                  </div>
                  <div class="row full-width hide" data-step="3" data-title="Sold by">
                    <div class="jumbotron">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span><img src="src/images/close.png" alt="close"></span></button>
                            <h4 class="modal-title" id="myModalLabel">Sold by</h4>
                        </div>           
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/rob-appliances.png" alt="Robinsons Appliances"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 15,300</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="my-cart.php" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        <div class="modal__partner">
                            <div class="row col-2">
                                <div class="column left modal__partner--item">
                                    <div class="modal__partner--item--logo"><img src="src/images/mega-saver.png" alt="Mega Saver"></div>
                                </div>
                                <div class="column right modal__partner--item">
                                    <div class="modal__partner--item--details">
                                        <span>PRICE</span>
                                        <p>₱ 21,500</p>
                                        <span>Delivery by</span>
                                        <p>Monday, December 25 2021</p>
                                        <a href="my-cart.php" class="btn btn__blue">Add to Cart</a>
                                    </div>
                                </div>                
                            </div> 
                        </div>
                        
                    </div>
                  </div>
                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    </div>
  </div>
</div>

<?php include 'src/includes/carrier-services.php';?>
<?php include 'src/includes/footer.php';?>


<script>
$('#myModal1').modalSteps();

// var span = document.getElementsByClassName("close")[0];

// $('.button').click(function(){
//   var buttonId = $(this).attr('id');
//   $('#modal-container').removeAttr('class').addClass(buttonId);
//   $('body').addClass('modal-active');
// })

// $('#modal-container').click(function(){
//   $(this).addClass('out');
//   $('body').removeClass('modal-active');
// });

$('#myModal1').modalSteps({
  btnCancelHtml: "Cancel",
  // btnPreviousHtml: "View Cart",
  btnNextHtml: "Check Out",
  btnLastStepHtml: "Complete",
  disableNextButton: false
  // completeCallback: function() {},
  // callbacks: {},
  // getTitleAndStep: function() {}
});
</script>