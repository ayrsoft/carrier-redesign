<?php include 'src/includes/header.php';?>

<section class="search-main">
    <div class="o-container">
        <div class="search-holder">
            <div class="search-holder__text">
                <h3>Search Results</h3>
            </div>
            <div class="search-holder__search">
                <p class="h6">2 Results for ‘Air Conditioner’</p>
                <form class="classic-form">
                    <div class="form-group with-search">
                        <input type="text" placeholder="Search">
                    </div>
                </form>
            </div>
        </div>
        <div class="search-content">
            <div class="search-content__result" style="display:block;"> <!-- Search result design -->
                <div class="cards">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-content__header">
                                <img src="src/images/product-2.png" alt="product">
                            </div>
                            <div class="card-content__body">
                                <span class="subtitle">Non-Inverter</span>
                                <div class="title">iCool Green Remote</div>
                                <div class="rate">
                                    <div class="rate-stars">
                                        <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                            <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                        </div>
                                    </div>
                                    <div class="rate-counts">(16)</div>
                                </div>
                                <span class="price">₱24,600.00</span>
                            </div>
                            <div class="card-content__footer">
                                <button class="o-button-full">Compare</button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-content__header">
                                <img src="src/images/product-2.png" alt="product">
                            </div>
                            <div class="card-content__body">
                                <span class="subtitle">Non-Inverter</span>
                                <div class="title">iCool Green Remote</div>
                                <div class="rate">
                                    <div class="rate-stars">
                                        <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                            <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                        </div>
                                    </div>
                                    <div class="rate-counts">(16)</div>
                                </div>
                                <span class="price">₱24,600.00</span>
                            </div>
                            <div class="card-content__footer">
                                <button class="o-button-full">Compare</button>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div class="card-content__header">
                                <img src="src/images/product-2.png" alt="product">
                            </div>
                            <div class="card-content__body">
                                <span class="subtitle">Non-Inverter</span>
                                <div class="title">iCool Green Remote</div>
                                <div class="rate">
                                    <div class="rate-stars">
                                        <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                            <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                            <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                        </div>
                                    </div>
                                    <div class="rate-counts">(16)</div>
                                </div>
                                <span class="price">₱24,600.00</span>
                            </div>
                            <div class="card-content__footer">
                                <button class="o-button-full">Compare</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-content__noresult" style="display:none;"> <!-- No result found design -->
                <h3>No results found.</h3>
                <p>We can’t find anything that matches your search ‘aircon conditioner’.</p>
                <div class="button-holder">
                    <a href="javascript:void(0)" class="o-button-full">Search again</a>
                    <a href="javascript:void(0)" class="o-button-default">Go to Shop</a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'src/includes/footer.php';?>