<?php include 'src/includes/header.php';?>
<?php include 'src/includes/breadcrumbs.php';?>
<section class="my-account">
        <div class="flex-content ">
            <?php include 'src/includes/my-account-sidebar.php';?>
            <?php include 'src/includes/my-account-info-card.php';?>
        </div>
</section>
<div class="footer-border-top"></div>
<?php include 'src/includes/footer.php';?>