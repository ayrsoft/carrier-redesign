<section class="carrier-services">
    <div class="o-container">
        <div class="carrier-services__main">
            <div class="carrier-services__main--holder support">
                <div class="services-content">
                    <p>Support 24/7</p>
                    <span>Ready to cater to your every need</span>
                    <a href="javascript:void(0)">Ask a question</a>
                </div>
            </div>
            <div class="carrier-services__main--holder tested">
                <div class="services-content">
                    <p>Tried. Tested. Trusted</p>
                    <span>Our rigorous tests and original parts ensure excellent quality</span>
                </div>
            </div>
            <div class="carrier-services__main--holder authorized">
                <div class="services-content">
                    <p>Carrier Authorized</p>
                    <span>Our skilled technicians undergo extensive training so your unit is in good hands</span>
                    <a href="javascript:void(0)">Visit Customer Care</a>
                </div>
            </div>
        </div>
    </div>
</section>