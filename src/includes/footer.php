    <?php include 'src/includes/carrier-services.php';?>
    <footer id="colophon" class="site-footer">
        <div class="o-container">
            <div class="site-footer__logo">
                <a href="/homepage"><img src="src/images/footer-logo.png" alt="logo"></a>
                <div class="social-fb-mob">
                    <span>Follow us on Facebook</span>
                </div>
            </div>
            <div class="site-footer__content">
                <div class="footer-address">
                    <p>Concepcion-Carrier Air Conditioning Company (CCAC) Km 20 East Service Road, South Superhighway, Alabang, Muntinlupa</p>
                </div>
                <div class="footer-menu">
                    <ul>
                        <li>Shop Our Products</li>
                        <li>Customer Care</li>
                    </ul>
                    <ul>
                        <li>Privacy Policy</li>
                        <li>Terms & Conditions</li>
                    </ul>
                </div>
                <div class="footer-newsletter">
                    <div class="site-footer__logo">
                        <div class="social-fb">
                            <span>Follow us on Facebook</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-footer__bottom">
                <div class="site-footer__bottom--holder">
                    <div class="copyrights">
                        <p>Copyright © 2020 Concepcion-Carrier Air-Conditioning Company. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
        <?php if($_SERVER['REQUEST_URI'] == '/product-detail.php'): ?>
            <div class="buy-product">
                <div class="o-container">
                    <form action="">
                        <div class="buy-product__holder">
                            <div class="buy-product__holder--name">
                                <h6>iCool Green Remote SD</h6>
                            </div>
                            <div class="buy-product__holder--details">
                                <p class="h6">PHP 25,000</p>
                                <button class="o-button-full">Buy Now</button>
                                <button type="button" class="share-social"></button>
                                <div class="share-tooltip" data-target-tooltip="show">
                                    <h6>Share this product</h6>
                                    <ul>
                                        <li><a href="https://www.facebook.com/" target="_blank"><img src="src/images/icons/share-fb.svg" alt="facebook"></a></li>
                                        <li><a href="https://twitter.com/" target="_blank"><img src="src/images/icons/share-twitter.svg" alt="twitter"></a></li>
                                        <li><a href="https://mail.google.com/mail/u/0/" target="_blank"><img src="src/images/icons/share-email.svg" alt="email"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </footer>
    <div class="modal-backgdrop" data-target="backdrop"></div>

    <!-- Cart Modal -->
    <div class="cart-holder" data-modal-holder="cart">
        <button class="modal-close" data-modal-close="dismiss">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M18 6L6 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M6 6L18 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </button>
        <div class="my-cart">
            <h6>My Cart (1 item)</h6>
        </div>
        <div class="my-cart-items">
            <div class="my-cart-items__holder">
                <div class="item-delete">
                    <img src="src/images/icons/trash.svg" alt="delete">
                </div>
                <div class="item-content">
                    <div class="item-content__description">
                        <img src="src/images/cart-1.svg" alt="cart">
                        <p>iCool Green Remote SD 0.5 HP x 1</p>
                    </div>
                    <div class="item-content__price">
                        ₱ 15,800
                    </div>
                </div>
            </div>
        </div>
        <div class="my-cart-total">
            <div class="my-cart-total__description">
                <label for="">SUBTotal</label>
                <h6>₱15,800</h6>
            </div>
            <button class="o-button-default">View Cart</button>
            <button class="o-button-full">Check Out</button>
        </div>
    </div>

    <!-- Write a Message Modal -->
    <div class="o-modal write-message-modal" data-modal-holder="write-review">
        <div class="o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke="#23419A" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <form id="review-submit" action="">
                <div class="review-message__holder">
                    <div class="review-message__holder--content active" data-form-review="initial">
                        <h3>Tell us about your Alpha Inverter.</h3>
                        <div class="rate">
                            <div class="rate-stars">
                                <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                    <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                    <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                </div>
                            </div>
                        </div>
                        <p>How was your experience?</p>
                        <div class="review-footer">
                            <span>YOUR REVIEW</span>
                            <textarea name="" id="" cols="30" rows="10" placeholder="How did you like your airpurifier? Would you recommend it to a friend?"></textarea>
                            <button class="o-button-full">Post Review</button>
                        </div>
                    </div>
                    <div class="thankyou-review" data-form-review="success">
                        <h3>Thank you for leaving a review!</h3>
                        <hr>
                        <p>Get the most out of your product by availing of our cleaning or repair services here:</p>
                        <div class="review-customer-care">
                            <div class="review-customer-care__image list-holder">
                                <svg width="45" height="44" viewBox="0 0 45 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0)">
                                    <path d="M24.7923 25.0055L36.7622 36.9753C36.8881 37.1012 37.0532 37.164 37.2181 37.164C37.3829 37.164 37.548 37.1012 37.6736 36.9753C37.9254 36.7236 37.9254 36.3157 37.6736 36.0639L25.7038 24.0941C25.452 23.8423 25.0438 23.8423 24.7923 24.0941C24.5406 24.3459 24.5406 24.7537 24.7923 25.0055Z"/>
                                    <path d="M14.5472 13.849C14.2955 14.1008 14.2955 14.5086 14.5472 14.7604L17.7803 17.9935C17.9062 18.1194 18.071 18.1821 18.2362 18.1821C18.401 18.1821 18.5658 18.1194 18.6917 17.9935C18.9435 17.7417 18.9435 17.3338 18.6917 17.0821L15.4586 13.849C15.2069 13.5972 14.799 13.5972 14.5472 13.849Z"/>
                                    <path d="M22.1709 25.0848L32.5519 13.596L33.3244 14.2895C33.5469 14.4889 33.8259 14.5873 34.1038 14.5873C34.4231 14.5873 34.7413 14.4577 34.9713 14.2033L35.1754 13.9773L35.9018 14.6336L35.6134 14.9525C35.2986 15.301 35.1384 15.7508 35.1623 16.2198C35.1861 16.6884 35.3909 17.1201 35.739 17.4347L37.489 19.0158C37.8146 19.3102 38.2292 19.4693 38.6649 19.4693C38.6951 19.4693 38.7257 19.4686 38.7559 19.4669C39.2248 19.4435 39.6562 19.2383 39.9711 18.8902L43.8547 14.5917C44.5043 13.8726 44.4479 12.7595 43.7292 12.1095L41.9792 10.5284C41.6311 10.2139 41.1813 10.0541 40.7123 10.0773C40.2672 10.0998 39.8556 10.2857 39.5454 10.6026L38.8183 9.94567L39.0805 9.65496C39.5122 9.1776 39.4749 8.43773 38.9976 8.00569L33.1813 2.74336V2.74302C31.6026 1.31666 29.5434 0.419347 27.3832 0.215917C25.2298 0.0131578 23.2055 0.51502 21.6832 1.62785C21.3008 1.90714 21.1256 2.38517 21.2364 2.84541C21.3475 3.30564 21.7214 3.65107 22.1891 3.72526C26.5947 4.42552 27.7911 5.84483 28.101 6.42256C28.4142 7.00667 28.394 7.66832 28.0392 8.44478C27.8794 8.79491 27.9026 9.18801 28.0845 9.50725L17.6777 21.0252C17.1439 20.7191 16.4494 20.8141 16.0203 21.2884L15.2482 22.1431C14.8169 22.6205 14.7951 23.3268 15.1603 23.8273L12.4717 25.729C12.4677 25.732 12.4637 25.7347 12.4596 25.7377C11.3774 26.5407 10.3703 27.4548 9.46692 28.4548L1.76039 36.9838C0.230301 38.677 0.363236 41.2995 2.05648 42.8296C2.82354 43.5228 3.80007 43.8977 4.82629 43.8977C4.89745 43.8977 4.96896 43.8961 5.0408 43.8924C6.14489 43.8366 7.16104 43.3539 7.90225 42.5335L15.6091 34.0045C16.5125 33.0048 17.3201 31.9104 18.0097 30.7523C18.012 30.7483 18.0147 30.7439 18.017 30.7399L19.6371 27.8727C19.8402 27.9922 20.0712 28.0553 20.3108 28.0553C20.3337 28.0553 20.3568 28.0547 20.38 28.0537C20.7365 28.0355 21.0648 27.8798 21.3042 27.6149L22.0763 26.7602C22.3156 26.4954 22.4375 26.1533 22.4193 25.7965C22.4062 25.5363 22.3196 25.2912 22.1709 25.0848ZM21.2347 24.1982L18.6545 21.8668L29.004 10.4126L31.5922 12.7353L21.2347 24.1982ZM40.7774 11.365C40.9026 11.3589 41.0222 11.4012 41.1151 11.4852L42.8648 13.0659C43.0565 13.2392 43.0716 13.5359 42.8983 13.7273L39.0147 18.0258C38.9308 18.1185 38.8156 18.1732 38.6908 18.1796C38.5662 18.1859 38.446 18.143 38.3534 18.0594L36.6034 16.4783C36.4117 16.305 36.3966 16.0086 36.5698 15.817L40.4538 11.5187C40.5377 11.4257 40.6526 11.3714 40.7774 11.365ZM38.6804 11.5583L36.7662 13.6772L36.0398 13.021L37.9542 10.9021L38.6804 11.5583ZM22.6862 2.50098C25.3677 0.752356 29.6169 1.26026 32.3166 3.69908L38.0432 8.88051L34.0958 13.2496L29.248 8.89964C29.7381 7.77876 29.7348 6.7408 29.2369 5.81294C28.1308 3.75077 24.7799 2.86253 22.6862 2.50098ZM16.8985 30.099C16.253 31.182 15.4973 32.2052 14.6524 33.1401L6.94586 41.6691C6.4356 42.234 5.73568 42.5664 4.97534 42.605C4.21499 42.6436 3.48553 42.3834 2.92055 41.8732C2.35592 41.3629 2.02358 40.663 1.98531 39.903C1.9467 39.1426 2.20653 38.4128 2.71679 37.8482L10.4236 29.3189C11.2686 28.384 12.2099 27.529 13.222 26.7774L16.1174 24.7296L18.6428 27.0117L16.8985 30.099ZM21.1195 25.8958L20.3474 26.7505C20.3444 26.7542 20.3343 26.7649 20.3145 26.7659C20.2947 26.7649 20.284 26.7572 20.2806 26.7539L16.2083 23.0747C16.1889 23.0569 16.1875 23.0266 16.205 23.0075L16.9771 22.1528C16.9949 22.1334 17.0247 22.132 17.0442 22.1495L21.1162 25.829C21.1199 25.832 21.1309 25.8421 21.132 25.8616C21.133 25.8814 21.1229 25.8925 21.1195 25.8958Z"/>
                                    <path d="M3.397 14.257C5.07245 15.6126 7.33201 16.1748 9.59593 15.8002C10.5228 15.6465 11.4835 15.9624 12.1657 16.6445L15.1846 19.663C15.3101 19.7889 15.4753 19.8517 15.6401 19.8517C15.8053 19.8517 15.9701 19.7889 16.096 19.663C16.3477 19.4113 16.3477 19.0031 16.096 18.7513L13.0774 15.7327C12.1015 14.7572 10.7215 14.307 9.38545 14.5283C7.48441 14.8432 5.59714 14.3789 4.20771 13.2546C2.62256 11.9723 1.73263 10.0212 1.79172 7.98962L5.67872 11.1344C6.76368 12.0122 8.36024 11.8437 9.23741 10.7588L11.023 8.55225C11.448 8.02655 11.643 7.36725 11.5722 6.69519C11.5013 6.02279 11.1727 5.41854 10.6473 4.99356L6.75999 1.84844C8.82685 1.34255 10.9931 1.86556 12.6061 3.2738C14.0244 4.51217 14.7928 6.40011 14.7139 8.45355C14.6673 9.67514 15.1231 10.8585 15.9647 11.7004L19.9058 15.6414C20.1575 15.8929 20.5654 15.8929 20.8172 15.6414C21.0689 15.3897 21.0689 14.9815 20.8172 14.7297L16.8761 10.789C16.2876 10.2002 15.9691 9.36698 16.0023 8.5029C16.096 6.05838 15.1671 3.79849 13.4537 2.30263C11.4369 0.54192 8.69728 -0.0633363 6.12552 0.684254C5.71598 0.803089 5.40949 1.1371 5.32523 1.55572C5.24064 1.97466 5.39472 2.402 5.72672 2.67089L9.83628 5.9956C10.0941 6.20407 10.2552 6.50049 10.2901 6.83047C10.3251 7.16012 10.2294 7.48373 10.0206 7.74155L8.23536 9.9484C7.80467 10.4801 7.02183 10.5631 6.48975 10.1324L2.38052 6.80765C2.04819 6.53875 1.59769 6.47766 1.20492 6.64752C0.81384 6.81738 0.551663 7.18597 0.520779 7.61063C0.334469 10.1646 1.4097 12.6491 3.397 14.257Z"/>
                                    <path d="M43.4076 37.3202L27.8288 21.7413C27.577 21.4899 27.1688 21.4899 26.9174 21.7413C26.6656 21.9931 26.6656 22.4013 26.9174 22.6531L42.4962 38.2316C43.4499 39.1856 43.4499 40.7379 42.4962 41.6916C41.5422 42.6453 39.9903 42.646 39.0369 41.6919L24.3503 27.0057C24.0985 26.7539 23.6903 26.7539 23.4386 27.0057C23.1871 27.2574 23.1871 27.6653 23.4386 27.9171L38.1252 42.6037C38.8533 43.3318 39.8097 43.6957 40.7661 43.6957C41.7225 43.6957 42.6795 43.3314 43.4076 42.603C44.8639 41.1467 44.8639 38.7768 43.4076 37.3202Z"/>
                                    </g>
                                    <defs>
                                    <clipPath id="clip0">
                                    <rect width="44" height="44" fill="white" transform="translate(0.5)"/>
                                    </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class="review-customer-care__content">
                                <h6>Customer Care</h6>
                                <p>Quickly track your transactions and your warranty online with Concepcion-Carrier Customer Care.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Registration Modal - Sign up -->
    <div class="o-modal registration-modal" data-modal-holder="sign-up">
        <div class="sign-up-modal o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="sign-up-holder">
                <div class="sign-up-create">
                    <h3>Create an account</h3>
                    <p>Already have an account? <a href="javascript:void(0)" class="a-line" data-open-account="login">Log in</a></p>
                    <hr>
                    <form action="#" id="registration-form" class="classic-form">
                        <div class="form-group">
                            <label for="">first name</label>
                            <input type="text" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <label for="">last name</label>
                            <input type="text" placeholder="Last Name">
                        </div>
                        <div class="radioButton-option">
                            <p>Sign up with</p>
                            <input type="radio" id="email" name="sign-up-with" value="email" checked registration-option="email">
                            <label for="email">Email</label>
                            <input type="radio" id="mobile" name="sign-up-with" value="mobile" registration-option="mobile">
                            <label for="mobile">Mobile Number</label>
                        </div>
                        <div class="form-group input-email active" data-input-option="email">
                            <label for="">email</label>
                            <input type="email" placeholder="email@gmail.com">
                        </div>
                        <div class="form-group input-mobile" data-input-option="mobile">
                            <label for="">MOBILE number</label>
                            <div>
                                <input type="tel" placeholder="+63" value="+63" readonly>
                                <input type="tel" placeholder="555-555-5555">
                            </div>
                        </div>
                        <div class="form-group input-password">
                            <label for="">password</label>
                            <input type="password" placeholder="Password">
                        </div>
                        <div class="form-group input-password">
                            <label for="">confirm password</label>
                            <input type="password" placeholder="Password">
                        </div>
                        <ul class="checklist-group__list">
                            <li>
                                <label class="container-checkbox">
                                    I agree to Carrier’s <a href="" class="a-line">Terms & Conditions</a> and <a href="">Privacy Policy.</a>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container-checkbox">
                                    I'd like to subscribe to your e-newsletter to receive news and promos from Carrier.
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                        <button class="o-button-full">Create account</button>
                    </form>
                </div>
                <div class="sign-up-info">
                    <div class="sign-up-info__image">
                        <img src="src/images/product-image.png" alt="sign up">
                    </div>
                    <div class="sign-up-info__text">
                        <p>Keep track of your Carrier units and service transactions all in one place</p>
                        <p>Save your details with us and complete transactions even faster</p>
                        <p>Stay up to date with the latest news and offers about Carrier</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Verify account - Email -->
    <div class="o-modal" data-modal-holder="verify-email">
        <div class="verify-account-modal o-modal-content verify-email">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="o-modal__details">
                <h3>Verify your account.</h3>
                <p>We sent a verification link to your email address. Please open this link to begin using your account.</p>
                <form action="">
                    <p>Didn’t receive a verifiction link? <a href="">Resend email.</a></p>
                </form>
            </div>
        </div>
    </div>

    <!-- Verify account - OTP -->
    <div class="o-modal" data-modal-holder="verify-otp">
        <div class="verify-account-modal o-modal-content verify-otp">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="o-modal__details">
                <h3>Verify your account.</h3>
                <p>We sent an 6-digit One-Time Pin (OTP) to +63 917 **** 329. Enter the OTP to get started.</p>
                <form action="" id="verify-mobile" class="classic-form">
                    <div class="form-otp">
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                    </div>
                    <p>Didn’t receive an OTP? <a href="">Resend OTP.</a></p>
                    <button class="o-button-full">Submit OTP</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Registration modal - success -->
    <div class="o-modal" data-modal-holder="sucess">
        <div class="registration-success o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="registration-success__content">
                <div class="thankyou-review">
                    <h3>You’ve successfully created your account!</h3>
                    <hr>
                    <div class="review-customer-care">
                        <div class="review-customer-care__image list-holder">
                            <svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M41.9777 38.5772H41.2445V19.4558C42.9002 18.3904 44 16.5327 44 14.4218C44 14.3096 43.9707 14.1994 43.9151 14.102L38.8007 5.15177C38.4415 4.52331 37.7687 4.13281 37.0448 4.13281H6.95518C6.23133 4.13281 5.55852 4.52323 5.1993 5.15177L0.0849063 14.102C0.0293047 14.1994 0 14.3097 0 14.4218C0 16.5327 1.09983 18.3904 2.7555 19.4558V38.5772H2.02228C1.66633 38.5772 1.37775 38.8657 1.37775 39.2217C1.37775 39.5776 1.66633 39.8662 2.02228 39.8662H41.9777C42.3338 39.8662 42.6222 39.5776 42.6222 39.2217C42.6222 38.8657 42.3337 38.5772 41.9777 38.5772ZM1.33349 15.0663H3.39977C3.75573 15.0663 4.0443 14.7777 4.0443 14.4218C4.0443 14.0658 3.75573 13.7772 3.39977 13.7772H1.75519L6.31855 5.79132C6.44884 5.56341 6.69273 5.42188 6.95518 5.42188H37.0448C37.3073 5.42188 37.5513 5.56341 37.6814 5.79132L42.2447 13.7772H40.5998C40.2438 13.7772 39.9553 14.0658 39.9553 14.4218C39.9553 14.7777 40.2438 15.0663 40.5998 15.0663H42.6664C42.3513 17.3509 40.3863 19.1161 38.0165 19.1161C35.6468 19.1161 33.6828 17.3507 33.3676 15.0663H37.8446C38.2006 15.0663 38.4891 14.7777 38.4891 14.4218C38.4891 14.0658 38.2006 13.7772 37.8446 13.7772H6.15579C5.79984 13.7772 5.51126 14.0658 5.51126 14.4218C5.51126 14.7777 5.79984 15.0663 6.15579 15.0663H10.6324C10.3171 17.3507 8.35304 19.1161 5.9834 19.1161C3.61359 19.1161 1.64863 17.3509 1.33349 15.0663ZM31.9878 15.0663C31.6726 17.3507 29.7085 19.1161 27.3389 19.1161C24.9691 19.1161 23.0051 17.3507 22.6899 15.0663H31.9878ZM21.3101 15.0663C20.9949 17.3507 19.0308 19.1161 16.6611 19.1161C14.2915 19.1161 12.3274 17.3507 12.0122 15.0663H21.3101ZM36.5109 38.5772H29.5334V36.4219H36.5109V38.5772ZM36.5109 35.1328H29.5334V23.333H36.5109V35.1328ZM39.9554 38.5772H37.8V22.6884C37.8 22.3325 37.5115 22.0439 37.1555 22.0439H28.8889C28.5329 22.0439 28.2444 22.3325 28.2444 22.6884V38.5772H4.04456V20.0809C4.65309 20.29 5.30484 20.4052 5.9834 20.4052C8.31188 20.4052 10.3339 19.0683 11.3223 17.1218C12.3106 19.0682 14.3327 20.4052 16.6611 20.4052C18.9896 20.4052 21.0116 19.0683 22 17.1218C22.9884 19.0682 25.0104 20.4052 27.3389 20.4052C29.6673 20.4052 31.6894 19.0683 32.6777 17.1218C33.6661 19.0682 35.6881 20.4052 38.0166 20.4052C38.6952 20.4052 39.3469 20.29 39.9554 20.081V38.5772Z"/>
                                <path d="M25.4447 22.0449H6.84473C6.48877 22.0449 6.2002 22.3335 6.2002 22.6895V35.7783C6.2002 36.1343 6.48877 36.4229 6.84473 36.4229H25.4447C25.8007 36.4229 26.0892 36.1343 26.0892 35.7783V22.6895C26.0892 22.3334 25.8007 22.0449 25.4447 22.0449ZM24.8002 35.1338H7.48926V23.334H24.8002V35.1338Z"/>
                            </svg>
                        </div>
                        <div class="review-customer-care__content">
                            <h6>Go to shop</h6>
                            <p>Quickly track your transactions and your warranty online with Concepcion-Carrier Customer Care.</p>
                        </div>
                    </div>
                    <div class="review-customer-care">
                        <div class="review-customer-care__image list-holder">
                            <svg width="45" height="44" viewBox="0 0 45 44" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clip-path="url(#clip0)">
                                <path d="M24.7923 25.0055L36.7622 36.9753C36.8881 37.1012 37.0532 37.164 37.2181 37.164C37.3829 37.164 37.548 37.1012 37.6736 36.9753C37.9254 36.7236 37.9254 36.3157 37.6736 36.0639L25.7038 24.0941C25.452 23.8423 25.0438 23.8423 24.7923 24.0941C24.5406 24.3459 24.5406 24.7537 24.7923 25.0055Z"/>
                                <path d="M14.5472 13.849C14.2955 14.1008 14.2955 14.5086 14.5472 14.7604L17.7803 17.9935C17.9062 18.1194 18.071 18.1821 18.2362 18.1821C18.401 18.1821 18.5658 18.1194 18.6917 17.9935C18.9435 17.7417 18.9435 17.3338 18.6917 17.0821L15.4586 13.849C15.2069 13.5972 14.799 13.5972 14.5472 13.849Z"/>
                                <path d="M22.1709 25.0848L32.5519 13.596L33.3244 14.2895C33.5469 14.4889 33.8259 14.5873 34.1038 14.5873C34.4231 14.5873 34.7413 14.4577 34.9713 14.2033L35.1754 13.9773L35.9018 14.6336L35.6134 14.9525C35.2986 15.301 35.1384 15.7508 35.1623 16.2198C35.1861 16.6884 35.3909 17.1201 35.739 17.4347L37.489 19.0158C37.8146 19.3102 38.2292 19.4693 38.6649 19.4693C38.6951 19.4693 38.7257 19.4686 38.7559 19.4669C39.2248 19.4435 39.6562 19.2383 39.9711 18.8902L43.8547 14.5917C44.5043 13.8726 44.4479 12.7595 43.7292 12.1095L41.9792 10.5284C41.6311 10.2139 41.1813 10.0541 40.7123 10.0773C40.2672 10.0998 39.8556 10.2857 39.5454 10.6026L38.8183 9.94567L39.0805 9.65496C39.5122 9.1776 39.4749 8.43773 38.9976 8.00569L33.1813 2.74336V2.74302C31.6026 1.31666 29.5434 0.419347 27.3832 0.215917C25.2298 0.0131578 23.2055 0.51502 21.6832 1.62785C21.3008 1.90714 21.1256 2.38517 21.2364 2.84541C21.3475 3.30564 21.7214 3.65107 22.1891 3.72526C26.5947 4.42552 27.7911 5.84483 28.101 6.42256C28.4142 7.00667 28.394 7.66832 28.0392 8.44478C27.8794 8.79491 27.9026 9.18801 28.0845 9.50725L17.6777 21.0252C17.1439 20.7191 16.4494 20.8141 16.0203 21.2884L15.2482 22.1431C14.8169 22.6205 14.7951 23.3268 15.1603 23.8273L12.4717 25.729C12.4677 25.732 12.4637 25.7347 12.4596 25.7377C11.3774 26.5407 10.3703 27.4548 9.46692 28.4548L1.76039 36.9838C0.230301 38.677 0.363236 41.2995 2.05648 42.8296C2.82354 43.5228 3.80007 43.8977 4.82629 43.8977C4.89745 43.8977 4.96896 43.8961 5.0408 43.8924C6.14489 43.8366 7.16104 43.3539 7.90225 42.5335L15.6091 34.0045C16.5125 33.0048 17.3201 31.9104 18.0097 30.7523C18.012 30.7483 18.0147 30.7439 18.017 30.7399L19.6371 27.8727C19.8402 27.9922 20.0712 28.0553 20.3108 28.0553C20.3337 28.0553 20.3568 28.0547 20.38 28.0537C20.7365 28.0355 21.0648 27.8798 21.3042 27.6149L22.0763 26.7602C22.3156 26.4954 22.4375 26.1533 22.4193 25.7965C22.4062 25.5363 22.3196 25.2912 22.1709 25.0848ZM21.2347 24.1982L18.6545 21.8668L29.004 10.4126L31.5922 12.7353L21.2347 24.1982ZM40.7774 11.365C40.9026 11.3589 41.0222 11.4012 41.1151 11.4852L42.8648 13.0659C43.0565 13.2392 43.0716 13.5359 42.8983 13.7273L39.0147 18.0258C38.9308 18.1185 38.8156 18.1732 38.6908 18.1796C38.5662 18.1859 38.446 18.143 38.3534 18.0594L36.6034 16.4783C36.4117 16.305 36.3966 16.0086 36.5698 15.817L40.4538 11.5187C40.5377 11.4257 40.6526 11.3714 40.7774 11.365ZM38.6804 11.5583L36.7662 13.6772L36.0398 13.021L37.9542 10.9021L38.6804 11.5583ZM22.6862 2.50098C25.3677 0.752356 29.6169 1.26026 32.3166 3.69908L38.0432 8.88051L34.0958 13.2496L29.248 8.89964C29.7381 7.77876 29.7348 6.7408 29.2369 5.81294C28.1308 3.75077 24.7799 2.86253 22.6862 2.50098ZM16.8985 30.099C16.253 31.182 15.4973 32.2052 14.6524 33.1401L6.94586 41.6691C6.4356 42.234 5.73568 42.5664 4.97534 42.605C4.21499 42.6436 3.48553 42.3834 2.92055 41.8732C2.35592 41.3629 2.02358 40.663 1.98531 39.903C1.9467 39.1426 2.20653 38.4128 2.71679 37.8482L10.4236 29.3189C11.2686 28.384 12.2099 27.529 13.222 26.7774L16.1174 24.7296L18.6428 27.0117L16.8985 30.099ZM21.1195 25.8958L20.3474 26.7505C20.3444 26.7542 20.3343 26.7649 20.3145 26.7659C20.2947 26.7649 20.284 26.7572 20.2806 26.7539L16.2083 23.0747C16.1889 23.0569 16.1875 23.0266 16.205 23.0075L16.9771 22.1528C16.9949 22.1334 17.0247 22.132 17.0442 22.1495L21.1162 25.829C21.1199 25.832 21.1309 25.8421 21.132 25.8616C21.133 25.8814 21.1229 25.8925 21.1195 25.8958Z"/>
                                <path d="M3.397 14.257C5.07245 15.6126 7.33201 16.1748 9.59593 15.8002C10.5228 15.6465 11.4835 15.9624 12.1657 16.6445L15.1846 19.663C15.3101 19.7889 15.4753 19.8517 15.6401 19.8517C15.8053 19.8517 15.9701 19.7889 16.096 19.663C16.3477 19.4113 16.3477 19.0031 16.096 18.7513L13.0774 15.7327C12.1015 14.7572 10.7215 14.307 9.38545 14.5283C7.48441 14.8432 5.59714 14.3789 4.20771 13.2546C2.62256 11.9723 1.73263 10.0212 1.79172 7.98962L5.67872 11.1344C6.76368 12.0122 8.36024 11.8437 9.23741 10.7588L11.023 8.55225C11.448 8.02655 11.643 7.36725 11.5722 6.69519C11.5013 6.02279 11.1727 5.41854 10.6473 4.99356L6.75999 1.84844C8.82685 1.34255 10.9931 1.86556 12.6061 3.2738C14.0244 4.51217 14.7928 6.40011 14.7139 8.45355C14.6673 9.67514 15.1231 10.8585 15.9647 11.7004L19.9058 15.6414C20.1575 15.8929 20.5654 15.8929 20.8172 15.6414C21.0689 15.3897 21.0689 14.9815 20.8172 14.7297L16.8761 10.789C16.2876 10.2002 15.9691 9.36698 16.0023 8.5029C16.096 6.05838 15.1671 3.79849 13.4537 2.30263C11.4369 0.54192 8.69728 -0.0633363 6.12552 0.684254C5.71598 0.803089 5.40949 1.1371 5.32523 1.55572C5.24064 1.97466 5.39472 2.402 5.72672 2.67089L9.83628 5.9956C10.0941 6.20407 10.2552 6.50049 10.2901 6.83047C10.3251 7.16012 10.2294 7.48373 10.0206 7.74155L8.23536 9.9484C7.80467 10.4801 7.02183 10.5631 6.48975 10.1324L2.38052 6.80765C2.04819 6.53875 1.59769 6.47766 1.20492 6.64752C0.81384 6.81738 0.551663 7.18597 0.520779 7.61063C0.334469 10.1646 1.4097 12.6491 3.397 14.257Z"/>
                                <path d="M43.4076 37.3202L27.8288 21.7413C27.577 21.4899 27.1688 21.4899 26.9174 21.7413C26.6656 21.9931 26.6656 22.4013 26.9174 22.6531L42.4962 38.2316C43.4499 39.1856 43.4499 40.7379 42.4962 41.6916C41.5422 42.6453 39.9903 42.646 39.0369 41.6919L24.3503 27.0057C24.0985 26.7539 23.6903 26.7539 23.4386 27.0057C23.1871 27.2574 23.1871 27.6653 23.4386 27.9171L38.1252 42.6037C38.8533 43.3318 39.8097 43.6957 40.7661 43.6957C41.7225 43.6957 42.6795 43.3314 43.4076 42.603C44.8639 41.1467 44.8639 38.7768 43.4076 37.3202Z"/>
                                </g>
                                <defs>
                                <clipPath id="clip0">
                                <rect width="44" height="44" fill="white" transform="translate(0.5)"/>
                                </clipPath>
                                </defs>
                            </svg>
                        </div>
                        <div class="review-customer-care__content">
                            <h6>Customer Care</h6>
                            <p>Quickly track your transactions and your warranty online with Concepcion-Carrier Customer Care.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Login - email -->
    <div class="o-modal" data-modal-holder="login">
        <div class="login-email o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="login-email__content">
                <div class="login-heading">
                    <h3>Log in</h3>
                    <p>No account yet? <a href="javascript:void(0)" class="a-line" data-open-account="sign-up">Create an account</a></p>
                </div>
                <hr>
                <form action="" class="classic-form">
                    <div class="radioButton-option">
                        <p>Login with</p>
                        <input type="radio" id="login-email" name="login-with" value="email" checked login-option="email">
                        <label for="login-email">Email</label>
                        <input type="radio" id="login-mobile" name="login-with" value="mobile" login-option="mobile">
                        <label for="login-mobile">Mobile Number</label>
                    </div>
                    <div class="form-group input-email active" data-login-option="email">
                        <label for="">email</label>
                        <input type="text" placeholder="email@gmail.com">
                    </div>
                    <div class="form-group input-mobile" data-login-option="mobile">
                        <label for="">MOBILE number</label>
                        <div>
                            <input type="tel" placeholder="+63" value="+63" readonly>
                            <input type="tel" placeholder="555-555-5555">
                        </div>
                    </div>
                    <div class="form-group input-password">
                        <label for="">password</label>
                        <input type="text" placeholder="Password">
                        <a href="javascript:void(0)" data-target-forgot="login">Forgot password?</a>
                    </div>
                    <button class="o-button-full">Log in</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Login - forgot password -->
    <div class="o-modal" data-modal-holder="forgot-password">
        <div class="forgot-password o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="forgot-password__content">
                <h3>Forgot your password?</h3>
                <form action="" id="forgot-to-retrieve-form" class="classic-form">
                    <div class="radioButton-option">
                        <input type="radio" id="password-email" name="forgot-with" value="email" checked forgot-option="email">
                        <label for="password-email">Email</label>
                        <input type="radio" id="password-mobile" name="forgot-with" value="mobile" forgot-option="mobile">
                        <label for="password-mobile">Mobile Number</label>
                    </div>
                    <div class="form-group input-email active" data-forgot-option="email">
                        <p>Please enter the email address you used to create your account so we can send you a link to reset your password.</p>
                        <label for="">email</label>
                        <input type="text" placeholder="email@gmail.com">
                    </div>
                    <div class="form-group input-mobile" data-forgot-option="mobile">
                        <p>Please enter the mobile number you used to create your account so we can send you an OTP for verification.</p>
                        <label for="">MOBILE number</label>
                        <div>
                            <input type="tel" placeholder="+63" value="+63" readonly>
                            <input type="tel" placeholder="555-555-5555">
                        </div>
                    </div>
                    <button class="o-button-full">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <!-- Login - forgot password email retrieve -->
    <div class="o-modal" data-modal-holder="email-retrieve">
        <div class="email-retrieve o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="email-retrieve__content">
                <h3>Retreive your password</h3>
                <p>We sent a link with instructions to retrieve your password to your email address. </p>
                <form class="classic-form">
                    <p>Didn’t receive the link? <a href="">Resend email.</a></p>
                </form>
            </div>
        </div>
    </div>

    <!-- Login - forgot password mobile otp -->
    <div class="o-modal" data-modal-holder="mobile-retrieve">
        <div class="password-retrieve verify-account-modal verify-otp o-modal-content">
            <button class="modal-close" data-modal-close="dismiss">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18 6L6 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M6 6L18 18" stroke-width="1.25" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </button>
            <div class="password-retrieve__content">
                <h3>Retrieve your password </h3>
                <p>We sent an 6-digit One-Time Pin (OTP) to +63 917 **** 329. Enter the OTP to retrieve your password.</p>
                <form action="" class="classic-form">
                    <div class="form-otp">
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                        <div class="form-group">
                            <input type="text" placeholder="-">
                        </div>
                    </div>
                    <p>Didn’t receive an OTP? <a href="">Resend OTP.</a></p>
                    <button class="o-button-full">Submit OTP</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>