<section class="explainer-section">
    <div class="o-container">
        <div class="explainer-holder">
            <div class="explainer-holder__product">
                <img src="src/images/carrier-alpha-inverter.png" alt="product">
            </div>
            <div class="explainer-holder__description">
                <div class="product-text">
                    <span>Carrier Alpha Inverter</span>
                    <h2>A Combination of Art & Comfort</h2>
                    <p>Loaded with special features, create a better air space for your home and protect your family from viruses and bacteria.</p>
                    <a href="javascript:void(0)" class="o-button-default-white">View Product</a>
                </div>
                <div class="product-specs">
                    <div class="product-specs__item">
                        <img src="src/images/specs-1.jpg" alt="product-specs">
                        <p>Temperature</p>
                    </div>
                    <div class="product-specs__item">
                        <img src="src/images/specs-2.jpg" alt="product-specs">
                        <p>Temperature</p>
                    </div>
                    <div class="product-specs__item">
                        <img src="src/images/specs-3.jpg" alt="product-specs">
                        <p>Temperature</p>
                    </div>
                    <div class="product-specs__item">
                        <img src="src/images/specs-4.jpg" alt="product-specs">
                        <p>Temperature</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>