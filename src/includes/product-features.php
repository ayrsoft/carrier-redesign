<section class="product-features">
    <div class="o-container">
        <div class="box">
            
            <div class="product-features__content">
                <span>Features</span>
                <h3>Electrostatic Filter: Beat airborne pollutants to enjoy uninterrupted comfort</h3>
                <p>An exclusive 8-in-1 air filter system that neutralizes free radicals, removes odor, molds, and fungi from the air</p>
                <div class="product-features__excerpt--desktop">
                    <span>Electrostatic Filter</span>
                    <p>Antiviral/antioxidant qualities with deodorizing features and an anti-bacterial property</p>
                </div>
            </div>
            <div class="product-features__image">
                <ul id="features-slider" class="slider product-features__slider product-features__image--mobile">
                    <li><img src="https://picsum.photos/1280/594" loading="lazy" alt="product-feature"></li>
                    <li><img src="https://picsum.photos/1280/594" loading="lazy" alt="product-feature"></li>
                </ul>
                
                <div class="product-features__pointer--desktop"></div>
                <div class="product-features__pointer"></div>
            </div>
            <div class="product-features__excerpt">
                <span>Electrostatic Filter</span>
                <p>Antiviral/antioxidant qualities with deodorizing features and an anti-bacterial property</p>
            </div>
        </div>
    </div>
</section>