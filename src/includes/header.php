
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:ital,wght@0,300;0,700;1,300&family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="/dist/main.js"></script>
    <link rel="stylesheet" href="/dist/main.css">
    <title>Carrier</title>
</head>
<body class="preload">
    <header class="for-modal" data-target="headerbackdrop">
        <section class="main-header">
            <div class="o-container">
                <div class="affix">
                    <div class="affix-content">
                        <a href="/">
                            <img src="src/images/carrier.png" alt="logo">
                        </a>
                        <div class="affix-content__menuMob">
                            <a href="javascript:void(0)">
                                <img src="src/images/icons/burger-menu.svg" alt="menu-logo">
                            </a>
                        </div>
                        <div class="affix-menu">
                            <nav>
                                <ul class="main-menu">
                                    <li data-hover-dropdown="shop-our-products" class="active">
                                        <a href="javascript:void(0)">Shop our Products</a>
                                        <div class="shop-our-products sub-menu-content">
                                            <div class="o-container">
                                                <div class="shop-our-products__holder">
                                                    <div class="shop-our-products__holder--content">
                                                        <h6><a href="javascript:void(0)">Air Conditioners</a></h6>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Portable Aircon</a></li>
                                                            <li><a href="javascript:void(0)">Smart + Cool</a></li>
                                                            <li><a href="javascript:void(0)">Window Type</a></li>
                                                            <li><a href="javascript:void(0)">High Wall</a></li>
                                                            <li><a href="javascript:void(0)">Floor Mounted</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="shop-our-products__holder--content">
                                                        <h6><a href="javascript:void(0)">Air Purifiers</a></h6>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Tabletop Air Purifier</a></li>
                                                            <li><a href="javascript:void(0)">Standing Air Purifier</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="shop-our-products__holder--content">
                                                        <h6><a href="javascript:void(0)">Home Appliances</a></h6>
                                                        <ul>
                                                            <li><a href="javascript:void(0)">Ice Maker</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="common-button-text">
                                                        <a href="javascript:void(0)">View all products</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Customer Care</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Buyer’s Guide</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Store Locator</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Blog</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="affix-icons">
                        <div class="affix-icons__holder">
                            <button class="button-holder" data-trigger-dropdown="search">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 7.04605C0 3.16096 3.16097 0 7.04608 0C10.9314 0 14.0922 3.16096 14.0922 7.04605C14.0922 8.75504 13.4806 10.3238 12.4649 11.5451L15.8095 14.8897C16.0636 15.1436 16.0636 15.5556 15.8095 15.8094C15.6824 15.9365 15.5159 16 15.3497 16C15.1832 16 15.0168 15.9365 14.8898 15.8094L11.5452 12.4648C10.3239 13.4806 8.75508 14.0921 7.04608 14.0921C3.16097 14.0921 0 10.9314 0 7.04605ZM1.30081 7.04608C1.30081 10.214 3.87816 12.7913 7.04608 12.7913C10.214 12.7913 12.7913 10.214 12.7913 7.04605C12.7913 3.87815 10.214 1.30081 7.04608 1.30081C3.87816 1.30081 1.30081 3.87818 1.30081 7.04608Z"/>
                                </svg>
                                <div class="affix-search">Search</div>
                            </button>
                            <div class="search-holder">
                                <div class="o-container">
                                    <div class="search-holder__content">
                                        <form class="classic-form">
                                            <div class="form-group">
                                                <input type="text" placeholder="What are you looking for?">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            // switch to false to see navbar with user logout status
                            $loggedIn = false;
                        ?>
                        <div class="affix-icons__holder affix-icon-login">
                            <button class="button-holder" data-trigger-dropdown="user">
                                <svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4.8335 4.08333C4.8335 2.6566 5.99009 1.5 7.41683 1.5C8.84356 1.5 10.0002 2.6566 10.0002 4.08333C10.0002 5.51007 8.84356 6.66667 7.41683 6.66667C5.99009 6.66667 4.8335 5.51007 4.8335 4.08333ZM7.41683 0C5.16167 0 3.3335 1.82817 3.3335 4.08333C3.3335 6.3385 5.16167 8.16667 7.41683 8.16667C9.67199 8.16667 11.5002 6.3385 11.5002 4.08333C11.5002 1.82817 9.67199 0 7.41683 0ZM4.08333 10C3.00037 10 1.96175 10.4302 1.19598 11.196C0.430207 11.9618 0 13.0004 0 14.0833V15.75V16.5H1.5V15.75V14.0833C1.5 13.3982 1.77217 12.7411 2.25664 12.2566C2.74111 11.7722 3.39819 11.5 4.08333 11.5H10.75C11.4351 11.5 12.0922 11.7722 12.5767 12.2566C13.0612 12.7411 13.3333 13.3982 13.3333 14.0833V15.75V16.5H14.8333V15.75V14.0833C14.8333 13.0004 14.4031 11.9618 13.6374 11.196C12.8716 10.4302 11.833 10 10.75 10H4.08333Z"/>
                                </svg>
                                <div class="affix-login">Log in</div>
                            </button>
                            <div class="login-holder">
                                <?php if($loggedIn): ?>
                                    <ul class="user-in">
                                        <li><a href="javascript:void(0)">My Account</a></li>
                                        <li><a href="javascript:void(0)">Wishlist (1)</a></li>
                                        <li><a href="javascript:void(0)">Orders</a></li>
                                        <li><a href="javascript:void(0)">Service Requests</a></li>
                                        <li><a href="javascript:void(0)">My Registered Air Conditioners</a></li>
                                        <li><a href="javascript:void(0)">Log out</a></li>
                                    </ul>
                                <?php else: ?>
                                    <ul class="user-out">
                                        <li><a href="javascript:void(0)" data-trigger-modal="login">Login</a></li>
                                        <li><a href="javascript:void(0)" data-trigger-modal="sign-up">Sign Up</a></li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="affix-icons__holder">
                            <button class="button-holder" data-trigger-modal="cart">
                                <svg width="20" height="19" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.75 0C0.335786 0 0 0.335786 0 0.75C0 1.16421 0.335786 1.5 0.75 1.5H3.46857L4.17542 5.0316C4.17884 5.05388 4.18325 5.07583 4.18859 5.09742L5.58104 12.0544L5.58114 12.055C5.69172 12.6111 5.99431 13.1106 6.43599 13.4661C6.87577 13.8202 7.42554 14.009 7.98988 14H16.0768C16.6411 14.009 17.1909 13.8202 17.6307 13.4661C18.0725 13.1105 18.3751 12.6108 18.4856 12.0544L18.4856 12.0545L18.4867 12.0488L19.7068 5.65083C19.8538 4.88047 19.2632 4.16667 18.479 4.16667H5.53205L4.81875 0.602807L4.6981 0H4.08333H0.75ZM7.05208 11.7611L5.83228 5.66667H18.1768L17.0144 11.7622L17.0139 11.7646C16.9716 11.9746 16.857 12.1633 16.6901 12.2977C16.5225 12.4326 16.3128 12.5043 16.0977 12.5001L16.0905 12.5H16.0833H7.98333H7.97615L7.96896 12.5001C7.75387 12.5043 7.54417 12.4326 7.37659 12.2977C7.209 12.1628 7.09421 11.9732 7.0523 11.7622L7.05208 11.7611ZM7.41683 17.3333C7.37081 17.3333 7.3335 17.3706 7.3335 17.4166C7.3335 17.4627 7.37081 17.5 7.41683 17.5C7.46285 17.5 7.50016 17.4627 7.50016 17.4166C7.50016 17.3706 7.46285 17.3333 7.41683 17.3333ZM5.8335 17.4166C5.8335 16.5422 6.54238 15.8333 7.41683 15.8333C8.29128 15.8333 9.00016 16.5422 9.00016 17.4166C9.00016 18.2911 8.29128 19 7.41683 19C6.54238 19 5.8335 18.2911 5.8335 17.4166ZM16.5833 17.3333C16.5373 17.3333 16.5 17.3706 16.5 17.4166C16.5 17.4627 16.5373 17.5 16.5833 17.5C16.6294 17.5 16.6667 17.4627 16.6667 17.4166C16.6667 17.3706 16.6294 17.3333 16.5833 17.3333ZM15 17.4166C15 16.5422 15.7089 15.8333 16.5833 15.8333C17.4578 15.8333 18.1667 16.5422 18.1667 17.4166C18.1667 18.2911 17.4578 19 16.5833 19C15.7089 19 15 18.2911 15 17.4166Z"/>
                                </svg>
                                <div class="affix-cart"></div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>