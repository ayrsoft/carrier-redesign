<?php include 'src/includes/breadcrumbs.php';?>
<section class="shop-section">
    <div class="o-container">
        <div class="heading-content">
            <div class="heading-content__description">
                <h2>High Wall Airconditioners</h2>
                <span>Ranging from 1.0-2.5 HP</span>
                <p>
                    Designed to provide uninterrupted comfort that reaches many sections of a room without spending too much. 
                    Think about maximizing energy efficiency. 
                    Think Carrier Split Type Air Conditioners.
                </p>
            </div>
            <!-- <div class="heading-content__image">
                <img src="/src/images/shop-hero.png" alt="shop">
            </div> -->
        </div>
        <div class="shop-products">
            <div class="shop-products__sortby">
                <label for="">Sort By: </label>
                <div class="select-container" data-dropdown="sb">
                    <div class="select-container__option--selected" data-dropdown-value="sb">
                        Price: Low to High
                    </div>
                    <ul>
                        <li>Price: Low to low</li>
                        <li>Price: Low to medium</li>
                        <li>Price: Low to High</li>
                    </ul>
                </div>
            </div>
            <div class="cards">
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/alpha-inverter.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">Alpha Inverter</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱25,000.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/optima-2.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">Optima 2</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱25,000.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/crystal-inverter.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">Crystal Inverter</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱25,000.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/XPower-gold-2-inverter.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">XPower Gold 2 Inverter</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱24,600.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/iCool Green Remote SD 0.5 HP.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">iCool Green Remote</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱24,600.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/product-2.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">iCool Green Remote Top</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱24,600.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/optima.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">Optima Green</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱24,600.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <div class="card-content__header">
                            <img src="src/images/iCool-green.png" alt="product">
                        </div>
                        <div class="card-content__body">
                            <span class="subtitle">Non-Inverter</span>
                            <div class="title">iCool Green</div>
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">(16)</div>
                            </div>
                            <span class="price">₱15,800.00</span>
                        </div>
                        <div class="card-content__footer">
                            <button class="o-button-full">Compare</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>