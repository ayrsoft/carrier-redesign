<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/dist/main.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:ital,wght@0,300;0,700;1,300&family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>    
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js" integrity="sha512-bE0ncA3DKWmKaF3w5hQjCq7ErHFiPdH2IGjXRyXXZSOokbimtUuufhgeDPeQPs51AI4XsqDZUK7qvrPZ5xboZg==" crossorigin="anonymous"></script> -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/flatly/bootstrap.min.css"> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
    <script src="/dist/modal-steps.min.js"></script>
    <script src="/dist/main.js"></script>
    <title>FE Skeleton</title>
</head>
<body>
    <header>
        <section class="main-header">
            <div class="o-container">
                <div class="affix">
                    <div class="affix-content">
                        <a href="/homepage.php">
                            <img src="src/images/carrier.png" alt="logo">
                        </a>
                        <div class="affix-menu">
                            <nav>
                                <ul>
                                    <li>
                                        <a href="">Shop our Products</a>
                                    </li>
                                    <li>
                                        <a href="">Customer Care</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="affix-icons buttons">
                        <div class="affix-icons__search">
                            Search
                        </div>
                        <div class="affix-icons__login">
                            Log in
                        </div>
                        <div id="add-to-cart" class="button affix-icons__cart" data-toggle="modal" data-target="#myModal1"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- <section class="shop-our-products">
            <div class="o-container">
                <div class="shop-our-products__holder">
                    <div class="shop-our-products__holder--content">
                        <h6>Air Conditioners</h6>
                        <ul>
                            <li><a href="">Portable Aircon</a></li>
                            <li><a href="">Smart + Cool</a></li>
                            <li><a href="">Window Type</a></li>
                            <li><a href="">High Wall</a></li>
                            <li><a href="">Floor Mounted</a></li>
                        </ul>
                    </div>
                    <div class="shop-our-products__holder--content">
                        <h6>Air Purifiers</h6>
                        <ul>
                            <li><a href="">Tabletop Air Purifier</a></li>
                            <li><a href="">Standing Air Purifier</a></li>
                        </ul>
                    </div>
                    <div class="shop-our-products__holder--content">
                        <h6>Home Appliances</h6>
                        <ul>
                            <li><a href="">Ice Maker</a></li>
                        </ul>
                    </div>
                    <div class="view-all-products">
                        <a href="">View all products</a>
                    </div>
                </div>
            </div>
        </section>
        <section class="customer-care-menu">
            <div class="o-container">
                <div class="customer-care-menu__holder">
                    <div class="cc-heading">
                        <span>Powered by Concepcare</span>
                        <div class="go-to-customer-care">
                            <a href="">Go to Customer Care</a>
                        </div>
                    </div>
                    <div class="cc-content">
                    </div>
                </div>
            </div>
        </section> -->
    </header>