<section class="hero-section hero-2-section" style="background-image: url(/src/images/hero-2.jpg);">
    <div class="o-container">
        <div class="hero-content">
            <h1>Bring comfort to your beginnings</h1>
            <p>Have cleaner and safer air for your home with our Alpha Inverter</p>
            <div class="hero-content__button">
                <a href="javascript:void(0)" class="o-button-full-white-before">View Alpha Inverter</a>
            </div>
        </div>
    </div>
    <div class="featured-product">
        <div class="o-container">
            <div class="featured-product__main">
                <div class="featured-product__holder">
                    <div class="featured-card">
                        <div class="featured-card__item">
                            <img src="/src/images/featured-1.png" alt="featured-image">
                        </div>
                        <div class="featured-card__description">
                            <span>NEW</span>
                            <h5>iCool Green Remote SD</h5>
                            <p>For calming and allergy-free air</p>
                        </div>
                    </div>
                </div>
                <div class="featured-product__holder">
                    <div class="featured-card">
                        <div class="featured-card__item">
                            <img src="/src/images/featured-2.png" alt="featured-image">
                        </div>
                        <div class="featured-card__description">
                            <span>NEW</span>
                            <h5>Tabletop Air Purifier</h5>
                            <p>For calming and allergy-free air</p>
                        </div>
                    </div>
                </div>
                <div class="featured-product__holder">
                    <div class="featured-card">
                        <div class="featured-card__item">
                            <img src="/src/images/featured-3.png" alt="featured-image">
                        </div>
                        <div class="featured-card__description">
                            <span>NEW</span>
                            <h5>iCool Green Remote SD</h5>
                            <p>For calming and allergy-free air</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>