<section class="four-product four-product-tabs">
    <div class="o-container">
        <div class="four-product__heading">
            <div class="four-product-button">
                <ul class="tabs-button">
                    <li><button>Best-sellers</button></li>
                    <li><button>Latest Promos</button></li>
                </ul>
            </div>
            <div class="common-button-text">
                <a href="javascript:void(0)">View all products</a>
            </div>
        </div>
        <div class="cards">
            <div class="card">
                <div class="card-content">
                    <div class="card-content__header">
                        <img src="src/images/product-2.png" alt="product">
                    </div>
                    <div class="card-content__body">
                        <span class="subtitle">Non-Inverter</span>
                        <div class="title">iCool Green Remote</div>
                        <div class="rate">
                            <div class="rate-stars"></div>
                            <div class="rate-counts">(16)</div>
                        </div>
                        <span class="price">₱24,600.00</span>
                    </div>
                    <div class="card-content__footer">
                        <button class="o-button-full">Compare</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <div class="card-content__header">
                        <img src="src/images/product-2.png" alt="product">
                    </div>
                    <div class="card-content__body">
                        <span class="subtitle">Non-Inverter</span>
                        <div class="title">iCool Green Remote</div>
                        <div class="rate">
                            <div class="rate-stars"></div>
                            <div class="rate-counts">(16)</div>
                        </div>
                        <span class="price">₱24,600.00</span>
                    </div>
                    <div class="card-content__footer">
                        <button class="o-button-full">Compare</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <div class="card-content__header">
                        <img src="src/images/product-2.png" alt="product">
                    </div>
                    <div class="card-content__body">
                        <span class="subtitle">Non-Inverter</span>
                        <div class="title">iCool Green Remote</div>
                        <div class="rate">
                            <div class="rate-stars"></div>
                            <div class="rate-counts">(16)</div>
                        </div>
                        <span class="price">₱24,600.00</span>
                    </div>
                    <div class="card-content__footer">
                        <button class="o-button-full">Compare</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <div class="card-content__header">
                        <img src="src/images/product-2.png" alt="product">
                    </div>
                    <div class="card-content__body">
                        <span class="subtitle">Non-Inverter</span>
                        <div class="title">iCool Green Remote</div>
                        <div class="rate">
                            <div class="rate-stars"></div>
                            <div class="rate-counts">(16)</div>
                        </div>
                        <span class="price">₱24,600.00</span>
                    </div>
                    <div class="card-content__footer">
                        <button class="o-button-full">Compare</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>