<section class="social-media">
    <div class="o-container">
        <div class="social-media__heading">
            <h5>Follow us for the latest updates</h5>
            <div class="social-media__heading--fb">
                <a href="javascript:void(0)">@CarrierPhilippines</a>
            </div>
        </div>
    </div>
</section>
<section class="social-media-content">
    <div class="social-media-content__holder">
        <div class="social-media-content__holder--img">
            <img src="src/images/social-media.png" alt="social-media">
        </div>
        <div class="social-media-content__holder--img">
            <img src="src/images/social-media-1.png" alt="social-media">
        </div>
        <div class="social-media-content__holder--img">
            <img src="src/images/social-media-2.png" alt="social-media">
        </div>
        <div class="social-media-content__holder--img">
            <img src="src/images/social-media-3.png" alt="social-media">
        </div>
    </div>
</section>