<section class="specifications">
    <div class="o-container">
        <div class="specifications__content">
            <h3>Specifications</h3>
            <p>You are viewing product specifications for the iCool Green Remote SD 0.5 HP</p>
        </div>
        <ul class="table-list">
            <li class="table-list__item">
                <button class="js-collapse">Physical</button>
                <ul class="table-list--inner">
                    <li class="table-list__item--inner">
                        <p>WIDTH X HEIGHT X DEPTH</p>    
                        <p>470x354x500mm</p>
                    </li>
                    <li class="table-list__item--inner">
                        <p>NET WEIGHT</p>    
                        <p>23 kg</p>
                    </li>
                </ul>
            </li>
            <li class="table-list__item">
                <button class="js-collapse">Capacity</button>
                <ul class="table-list--inner">
                    <li class="table-list__item--inner">
                        <p>ESTIMATED ENERGY CONSUMPTION (PHP/HOUR)</p>    
                        <p>₱1.50/hour</p>
                    </li>
                    <li class="table-list__item--inner">
                        <p>RECOMMENDED COOLING AREA</p>    
                        <p>8 to 10 sqm</p>
                    </li>
                    <li class="table-list__item--inner">
                        <p>COOLING CAPACITY</p>    
                        <p>5,275 kJ/hr</p>
                    </li>
                    <li class="table-list__item--inner">
                        <p>POWER CONSUMPTION</p>    
                        <p>527 WATTS</p>
                    </li>
                    <li class="table-list__item--inner">
                        <p>ENERGY EFFICIENT RATIO</p>    
                        <p>10 kJ/W-hr</p>
                    </li>
                </ul>
            </li>
            <li class="table-list__item">
                <button class="js-collapse">Noise</button>
                <ul class="table-list--inner">
                    <li class="table-list__item--inner">
                        <p>SOUND LEVEL (at Low)</p>    
                        <p>48dB</p>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</section>
