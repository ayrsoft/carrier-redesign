<section class="buyers-guide">
    <div class="o-container">
        <div class="buyers-guide__content">
            <span>BUYER’S GUIDE SURVEY</span>
            <h3>Find the airconditioner for you</h3>
            <p>We believe in making smart purchasing decisions. Take this survey to help you choose the right airconditioner that checks everything on your list. For advice from an Airconditioner Specialist for bigger purchases, request for a Full Service Consultation.</p>
            <a href="javascript:void(0)" class="o-button-default-white">Take the survey</a>
        </div>
    </div>
</section>