<section class="type-of-space">
    <div class="o-container">
        <div class="type-of-space__text">
            <span>Shop based on your type of space</span>
            <h4>Our wide selection for any type of space</h4>
            <p>Shop for air conditioners and purifiers for small to large bedrooms, living rooms, offices, or larger commercial spaces.</p>
        </div>
        <div class="glide">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide">0</li>
                    <li class="glide__slide">1</li>
                    <li class="glide__slide">2</li>
                </ul>
            </div>
        </div>
    </div>
</section>