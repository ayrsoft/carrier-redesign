<section class="reviews">
    <div class="o-container">
        <div class="reviews__title">
            <h3>Reviews</h3>
            <div class="rate">
                <div class="rate-stars">
                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                        
                        <div class="rate-counts">
                            <h6 class="rating">4</h6>/<h6>5</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cards">
            <div class="card">
                <div class="card__content--reviews">

                    <div class="rate">
                        <div class="rate-stars">
                            <div class="Rating" aria-label="Rating of this item is 4 out of 5">
                                <div class="rate-counts">
                                    <h6 class="rating">4</h6>/<h6>5</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="customer-name">Angel Cedro</span>

                    <p>Product is great, very convenient if you are not allowed to install a window or split type aircon in your place; although it was not mentioned that this product was noisy and will take a lot of getting used to.</p>

                    <span>posted jan 24, 2020</span>

                </div>
            </div>
            <div class="card">
                <div class="card__content--reviews">

                    <div class="rate">
                        <div class="rate-stars">
                            <div class="Rating" aria-label="Rating of this item is 4 out of 5">
                                <div class="rate-counts">
                                    <h6 class="rating">4</h6>/<h6>5</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="customer-name">Angel Cedro</span>

                    <p>Product is great, very convenient if you are not allowed to install a window or split type aircon in your place; although it was not mentioned that this product was noisy and will take a lot of getting used to.</p>

                    <span>posted jan 24, 2020</span>

                </div>
            </div>
            <div class="card">
                <div class="card__content--reviews">

                    <div class="rate">
                        <div class="rate-stars">
                            <div class="Rating" aria-label="Rating of this item is 4 out of 5">
                                <div class="rate-counts">
                                    <h6 class="rating">4</h6>/<h6>5</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span class="customer-name">Angel Cedro</span>

                    <p>Product is great, very convenient if you are not allowed to install a window or split type aircon in your place; although it was not mentioned that this product was noisy and will take a lot of getting used to.</p>

                    <span>posted jan 24, 2020</span>

                </div>
            </div>
        </div>
        <div class="pagination">
            <ul>
                <li class="no-previous">
                    <a href="javascript:void(0)">
                        <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 9L5 5L1 1" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                    </a>
                </li>
                <li class="active"><a href="javascript:void(0)">1</a></li>
                <li><a href="javascript:void(0)">2</a></li>
                <li><a href="javascript:void(0)">3</a></li>
                <li><a href="javascript:void(0)">4</a></li>
                <li>
                    <a href="javascript:void(0)">
                        <svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 9L5 5L1 1" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"></path>
                        </svg>
                    </a>
                </li>
            </ul>
        </div>
        <div class="cta">
            <a href="javascript:void(0)" class="o-button-full" title="Write Review">Write a review</a>
        </div>
    </div>
</section>