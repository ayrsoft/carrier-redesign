<section class="header-text">
    <div class="o-container">
        <div class="header-text__content">
            <h1>The Science of Indoor Living</h1>
            <p>Carrier exists to make indoor environments places where people can thrive. <span>Let us help you get started</span></p>
        </div>
    </div>
</section>