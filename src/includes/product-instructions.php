<section class="product-instructions">
    <div class="o-container--full">

        <div class="product-instructions__title">
            <h3>Installing your Portable Aircon</h2>
            <p>Need assitance? <a href="javascript:void(0)" title="Request Installation">Request for installation</a></p>
        </div>
        
        <ul id="installation-slider">
            
            <li>
                <img src="https://picsum.photos/634/426" loading="lazy" alt="step-1">
                
                <div class="step">
                    <div>
                        <span class="step__current">1</span> / <span class="step__total">5</span>	
                    </div>
                    <h5>Attach Energy Savings Plug</h5>
                    <p>With the aircon switched off, unplug the power cord from the wall outlet. Take the aircon's power cord then connect it with the Smart+Cool plug. Gently insert the Smart+Cool plug (with the attached aircon power cord) into the wall outlet. Avoid plugging into an extension cord.</p>
                </div>
            </li>

            <li>
                <img src="https://picsum.photos/634/426" loaidng="lazy" alt="step-2">
                
                <div class="step">
                    <div>
                        <span class="step__current">2</span> / <span class="step__total">5</span>	
                    </div>
                    <h5>Attach Energy Savings Plug</h5>
                    <p>With the aircon switched off, unplug the power cord from the wall outlet. Take the aircon's power cord then connect it with the Smart+Cool plug. Gently insert the Smart+Cool plug (with the attached aircon power cord) into the wall outlet. Avoid plugging into an extension cord.</p>
                </div>
            </li>
        
            
        </ul>


    </div>
</section>