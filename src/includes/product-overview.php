<section> <!-- This is for temporary only! Please see the breadcrumbs.php. -->
    <div class="o-container">
        <div id="breadcrumbs">
            <ul class="breadcrumbs-list">
                <li><a href="javascript:void(0)">Home</a></li>
                <li><a href="javascript:void(0)">Shop</a></li>
                <li><a href="javascript:void(0)">Alpha Inverter</a></li>
            </ul>
        </div>
    </div>
</section>
<section>
    <div class="product-overview">
        <div class="product-detail">
            <ul class="product-detail-list">
                <li><a href="javascript:void(0)">overview</a></li>
                <li><a href="javascript:void(0)">specifications</a></li>
                <li><a href="javascript:void(0)">features</a></li>
                <li><a href="javascript:void(0)">installation</a></li>
                <li><a href="javascript:void(0)">reviews</a></li>
                <li><a href="javascript:void(0)">similar products</a></li>
            </ul>
        </div>
        <div class="o-container">
            <div class="product-holder">
                <div class="product-image">
                    <div class="product-image__lists">
                        <div class="product-image__lists--slider">
                            <img src="src/images/product-image.png" alt="product-image" data-preview="photo">
                            <div class="slider-control">
                                <button type="button" data-gallery="previous"><img src="src/images/icons/slider-arrow-blue.svg" alt="product"></button>
                                <button type="button" data-gallery="next"><img src="src/images/icons/slider-arrow-blue.svg" alt="product"></button>
                            </div>
                        </div>
                        <div class="product-image__lists--info">
                            <ul>
                                <li class="active-gallery"><button type="button" data-gallery="view-photo"><img src="src/images/product-image.png" alt="product"></button></li>
                                <li><button type="button" data-gallery="view-photo"><img src="src/images/product-5.jpg" alt="product"></button></li>
                                <li><button type="button" data-gallery="view-photo"><img src="src/images/product-6.jpg" alt="product"></button></li>
                                <li><button type="button" data-gallery="view-photo"><img src="src/images/product-6.jpg" alt="product"></button></li>
                                <li><button type="button" data-gallery="view-photo"><img src="src/images/product-6.jpg" alt="product"></button></li>
                            </ul>
                        </div>
                    </div>
                    <div class="product-image__footer">
                        <div class="product-image__footer--holder">
                            <div class="product-includes">
                                <h6>What’s in the box?</h6>
                                <ul>
                                    <li>One (1) AC unit</li>
                                    <li>One (1) Remote control</li>
                                    <li>One (1) user manual with warranty card</li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-image__footer--share">
                            <h6>Share this product</h6>
                            <ul>
                                <li><a href="https://www.facebook.com/" target="_blank"><img src="src/images/icons/share-fb.svg" alt="facebook"></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><img src="src/images/icons/share-twitter.svg" alt="twitter"></a></li>
                                <li><a href="https://mail.google.com/mail/u/0/" target="_blank"><img src="src/images/icons/share-email.svg" alt="email"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="product-description">
                    <div class="product-description__title">
                        <span>BEST-SELLER</span>
                        <h3>iCool Green Remote SD</h3>
                        <p class="h6">PHP 32,000</p>
                    </div>
                    <div class="product-description__detail">
                        <span>CAUN026LC1</span>
                        <div class="write-review">
                            <div class="rate">
                                <div class="rate-stars">
                                    <div class="Rating" aria-label="Rating of this item is 3 out of 5">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star Rating--Star__active">
                                        <img src="/src/images/icons/star-active.svg" class="Rating--Star">
                                        <img src="/src/images/icons/star-default.svg" class="Rating--Star">
                                    </div>
                                </div>
                                <div class="rate-counts">16 Reviews</div>
                            </div>
                            <button data-trigger-modal="write-review">Write a review</button>
                        </div>
                        <p>Free yourself from worrying about allergens and free radicals in the air you breathe because Carrier iCool Green Timer Aircon does the job for you. 8 in 1 Air Filter System available across all iCool Green Deluxe models and iCool Green Remote side discharge models (1.0hp to 2.5hp)</p>
                        <ul class="product-info-list">
                            <li>8-in-1 Air Filter System</li>
                            <li>Energy Savings Plug</li>
                            <li>R410A: non-ozone depleting refrigerant</li>
                        </ul>
                        <div class="select-container" data-dropdown="po">
                            <div class="select-container__option--selected" data-dropdown-value="po">
                                0.5 HP
                            </div>
                            <ul>
                                <li class="c-custom-select__option" data-drop-value="default">0.5 HP (up to 10sqm)</li>
                                <li class="c-custom-select__option">0.75 HP (up to 13 sqm)</li>
                                <li class="c-custom-select__option">1 HP (up to 17 sqm)</li>
                                <li class="c-custom-select__option">1.5 HP (up to 23 sqm)</li>
                                <li class="c-custom-select__option"> 2.5 HP (up to 41 sqm)</li>
                            </ul>
                            <input class="js-custom-selector__value" type="hidden" value />
                        </div>
                        <div class="product-description__detail--color">
                            <span>COLOR</span>
                            <div class="fill-color">
                                <button class="grey"></button>
                                <button class="dark-grey"></button>
                            </div>
                        </div>
                        <div class="product-description__detail--button">
                            <button class="o-button-full">Buy Now</button>
                            <button class="o-button-default">Add to Cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-logistic">
        <div class="o-container">
            <div class="product-logistic__holder">
                <div class="logistic-item">
                    <div class="logistic-item__card">
                        <h6>Warranty duration</h6>
                        <ul>
                            <li>- For parts and labor: 1 year</li>
                            <li>- Compressor: 5 years</li>
                            <li>- General Cleaning (within 1 year)</li>
                            <li>- Free General Cleaning</li>
                        </ul>
                    </div>
                </div>
                <div class="logistic-item">
                    <div class="logistic-item__card">
                        <h6>Shipping</h6>
                        <ul>
                            <li>For our shipping policy. <a href="javascript:void(0)">Read more</a></li>
                        </ul>
                    </div>
                </div>
                <div class="logistic-item">
                    <div class="logistic-item__card">
                        <h6>Return Policy</h6>
                        <ul>
                            <li>Valid for 1 year. <a href="javascript:void(0)">Read more</a></li>
                        </ul>
                    </div>
                </div>
                <div class="logistic-item">
                    <div class="logistic-item__card">
                        <h6>Online partners</h6>
                        <ul>
                            <li>For other buying options, </br><a href="javascript:void(0)">View other sellers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>