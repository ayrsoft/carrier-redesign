// Import your JS components here
import { header } from './components/header.js';
import { product } from './components/product.js';
import { slider } from './components/slider.js';
import { collapse } from './components/collapse.js';
import { allmodal } from './components/allmodal.js';
class App {
  constructor() {
    this.components = [];
  }

  component(component) {
    this.components.push(component);
  }

  boot() {
    this.components.forEach((component) => component.init());
  }
}

const app = new App();
// Push imported JS to component variable using .component method
app.component(new header());
app.component(new product());
app.component(new slider());
app.component(new collapse());
app.component(new allmodal());

document.addEventListener("DOMContentLoaded", () => app.boot());