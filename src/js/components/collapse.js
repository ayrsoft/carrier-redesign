

export class collapse {
    constructor() {
        this.buttonCollapse = [];
    }

    getElements() {
        this.buttonCollapse = document.querySelectorAll('button.js-collapse');
    }

    bindElements() {
        
        this.buttonCollapse.forEach(el => { 
            el.addEventListener('click', event => {
                let x = el.classList.contains('open');
                if (x) {
                    el.classList.remove('open');
                    el.nextElementSibling.style.display = "block";
                } else {
                    el.classList.add('open');
                    el.nextElementSibling.style.display = "none";
                }
            });
        });
    }

    init() {
        this.getElements();
        this.bindElements();

    }
}