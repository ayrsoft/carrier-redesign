import noUiSlider from 'nouislider';
import 'nouislider/distribute/nouislider.css';

export class product {
    constructor() {
        this.slider = null;
        this.sliderPoints = null;
        this.dataDropdown = null;
        this.sliderOnePoint = null;
        this.slideFixed = null;
        this.clearFilter = null;
        this.galleryTrigger = null;
        this.previewPhoto = null;
        this.postReview = null;
    }

    getElements() {
        this.slider = document.getElementById('slider');
        this.sliderPoints = document.querySelectorAll('[data-slide]');
        this.dataDropdown = document.querySelectorAll('[data-dropdown]');
        this.sliderOnePoint = document.getElementById('sliderOnePoint');
        this.slideFixed = document.querySelector('[data-slide-fixed]');
        this.clearFilter = document.querySelectorAll('[data-action="clear-filter"]');
        this.galleryTrigger = document.querySelectorAll('[data-gallery]');
        this.previewPhoto = document.querySelector('[data-preview="photo"]');
        this.postReview = document.getElementById('review-submit');
    }

    bindElements() {
        let _self = this;
        if (_self.slider != null) {
            noUiSlider.create(_self.slider, {
                start: [10, 14],
                connect: true,
                range: {
                    'min': 0,
                    'max': 45
                }
            });
        }
        if (_self.sliderPoints != null && _self.sliderPoints.length) {
            _self.slider.noUiSlider.on('update', function (values) {
                values.forEach( (value, key) => {
                    document.querySelector(`[data-slide="${key}"]`).innerHTML = `${value} sqm`;
                });
            });
        }

        if (_self.dataDropdown != null  && _self.dataDropdown.length) {
            _self.dataDropdown.forEach(element => {
                element.addEventListener('click', function () {
                    const elemClass = this.classList;
                    if (this.classList.contains('active')) {
                        this.classList.remove('active');
                    } else {
                        this.classList.add('active');
                    }
                    const childNodesLi = element.querySelectorAll('li');
                    childNodesLi.forEach(liElement => {
                        liElement.addEventListener('click', function () {
                            this.parentNode.parentNode.querySelector('[data-dropdown-value]').innerText = this.innerText;
                        });
                    });
                });
            });
        }

        if (_self.sliderOnePoint != null) {
            
            noUiSlider.create(_self.sliderOnePoint, {
                start: [25000],
                connect: 'lower',
                range: {
                    'min': 15000,
                    'max': 100000
                }
            });
            _self.sliderOnePoint.noUiSlider.on('update', function (values) {
                _self.slideFixed.innerText = `₱ ${new Number(Math.floor(values)).toLocaleString("US")}`;
            });
        }

        if (_self.clearFilter != null && _self.clearFilter.length) {
            _self.clearFilter.forEach(element => {
                element.addEventListener('click', function () {
                    document.querySelectorAll('[data-drop-value="default"]').forEach(elem => {
                        const value = elem.innerHTML;
                        elem.parentNode.parentNode.querySelector('[data-dropdown-value]').innerText = value;
                    });
                    document.querySelectorAll('[type="checkbox"]').forEach(elem => {
                        elem.checked = false; 
                    });
                    _self.slider.noUiSlider.set([10,14]);
                    _self.sliderOnePoint.noUiSlider.set(25000);
                })
            })
        }
        
        if (_self.galleryTrigger != null && _self.galleryTrigger.length) {
            _self.galleryTrigger.forEach(element => {
                if (element.getAttribute('data-gallery') == 'view-photo') {
                    element.addEventListener('click', function () {
                        const activeGallery = document.querySelector('.active-gallery');
                        if (activeGallery.classList.contains('active-gallery')) {
                            activeGallery.classList.remove('active-gallery');
                        }
                        this.parentNode.classList.add('active-gallery');
                        _self.setPreviewPhoto(this.parentNode);
                    });
                } else {
                    element.addEventListener('click', function () {
                        let attrib = this.getAttribute('data-gallery');
                        const activeGallery = document.querySelector('.active-gallery');
                        let sibling = null;
                        if (attrib == 'previous') {
                            sibling = activeGallery.previousElementSibling;
                            if (sibling == null) {
                                sibling = activeGallery.parentNode.lastElementChild;
                            }
                        } else {
                            sibling = activeGallery.nextElementSibling;
                            if (sibling == null) {
                                sibling = activeGallery.parentNode.firstElementChild;
                            }
                            console.log(sibling);
                        }
                        activeGallery.classList.remove('active-gallery');
                        sibling.classList.add('active-gallery');
                        _self.setPreviewPhoto(sibling);
                    });
                }
            })
        }

        if(_self.postReview != null) {
            _self.postReview.addEventListener('submit', function(e){
                e.preventDefault();
                const dataReview = document.querySelectorAll('[data-form-review]');
                dataReview.forEach(element => {
                    const dataValue = element.getAttribute('data-form-review');
                    if(dataValue == 'initial') {
                        element.classList.remove('active');
                    } else {
                        element.classList.add('active');
                    }
                });
            });
        }
    }

    setPreviewPhoto (activeGallery) {
        let imgUrl = null;
        let _self = this;
        imgUrl = activeGallery.querySelector('img').getAttribute('src');
        _self.previewPhoto.setAttribute('src', imgUrl);
    }

    init() {
        this.getElements();
        this.bindElements();

    }
}