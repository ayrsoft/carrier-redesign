export class allmodal {
    constructor() {
        this.regisForm = null;
        this.verifyOTP = null;
        this.registrationOption = null;
        this.loginOption = null;
        this.forgotPassword = null;
        this.globalOptionValue = 'email';
        this.forgotOption= null;
        this.forgotToRetrieveForm = null;
        this.openAccountAuth = null;
    }

    getElements() {
        this.regisForm = document.getElementById('registration-form');
        this.verifyOTP = document.getElementById('verify-mobile');
        this.registrationOption = document.querySelectorAll('[registration-option]');
        this.loginOption = document.querySelectorAll('[login-option]');
        this.forgotPassword = document.querySelector('[data-target-forgot]');
        this.forgotOption = document.querySelectorAll('[forgot-option]');
        this.forgotToRetrieveForm = document.getElementById('forgot-to-retrieve-form');
        this.openAccountAuth = document.querySelectorAll('[data-open-account]');
    }
    
    bindElements() {
        let _self = this;

        if(_self.regisForm != null) {
            _self.regisForm.addEventListener('submit', function(e) {
                e.preventDefault();
                let optionName = this.querySelector('[name="sign-up-with"]:checked').value;
                
                if(optionName == 'email') {
                    document.querySelector('[data-modal-holder="verify-email"]').classList.add('active');
                } else {
                    document.querySelector('[data-modal-holder="verify-otp"]').classList.add('active');
                }
                document.querySelector('[data-modal-holder="sign-up"]').classList.remove('active');
            });
        }

        if(_self.verifyOTP != null) {
            _self.verifyOTP.addEventListener('submit', function(e) {
                e.preventDefault();

                document.querySelector('[data-modal-holder="sucess"]').classList.add('active');
                document.querySelector('[data-modal-holder="verify-otp"]').classList.remove('active');
            });
        }

        if(_self.registrationOption != null) {
            _self.registrationOption.forEach(element => {
                element.addEventListener('click', function() {
                    _self.globalOptionValue = document.querySelector('[name="sign-up-with"]:checked').value;
                    let optionInput = document.querySelectorAll('[data-input-option]');

                    optionInput.forEach(elementOption => {
                        elementOption.classList.remove('active');
                    });

                    document.querySelector('[data-input-option="'+_self.globalOptionValue+'"]').classList.add('active');
                });
            });
        }

        if(_self.loginOption != null) {
            _self.loginOption.forEach(element => {
                element.addEventListener('click', function() {
                    _self.globalOptionValue = document.querySelector('[name="login-with"]:checked').value;
                    let optionLoginInput = document.querySelectorAll('[data-login-option]');

                    optionLoginInput.forEach(elementOption => {
                        elementOption.classList.remove('active');
                    });

                    document.querySelector('[data-login-option="'+_self.globalOptionValue+'"]').classList.add('active');
                    _self.changeForgotRadio();
                    
                });
            });
        }

        if(_self.forgotPassword != null) {
            _self.forgotPassword.addEventListener('click', function(e) {
                e.preventDefault();
                
                document.querySelector('[data-modal-holder="forgot-password"]').classList.add('active');
                document.querySelector('[data-modal-holder="login"]').classList.remove('active');
            });
        }

        if (_self.forgotOption != null && _self.forgotOption.length) {
            _self.forgotOption.forEach(elem => {
                elem.addEventListener('click', function () {
                    _self.globalOptionValue = document.querySelector('[name="forgot-with"]:checked').value;
                    _self.changeForgotRadio();
                });
            })
        }

        if (_self.forgotToRetrieveForm != null) {
            _self.forgotToRetrieveForm.addEventListener('submit', function (e) {
                e.preventDefault();

                document.querySelector(`[data-modal-holder="${_self.globalOptionValue}-retrieve"]`).classList.add('active');
                document.querySelector('[data-modal-holder="forgot-password"]').classList.remove('active');
            });
        }

        if (_self.openAccountAuth != null && _self.openAccountAuth.length) {
            _self.openAccountAuth.forEach(elem => {
                elem.addEventListener('click', function() {
                    const dataValue = this.getAttribute('data-open-account');
                    document.querySelector(`[data-modal-holder="${dataValue != 'login' ? 'login' : 'sign-up'}"]`).classList.remove('active');
                    document.querySelector(`[data-modal-holder="${dataValue}"]`).classList.add('active');
                });
            })
        }
    }

    changeForgotRadio () {
        let _self = this;
        document.querySelector(`[forgot-option="${_self.globalOptionValue}"]`).checked = true;
        const dataForgotOption = document.querySelectorAll(`[data-forgot-option]`);

        dataForgotOption.forEach(elem => {
            elem.classList.remove('active');
        });
        document.querySelector(`[data-forgot-option="${_self.globalOptionValue}"]`).classList.add('active');
    }

    init() {
        this.getElements();
        this.bindElements();
    }
}