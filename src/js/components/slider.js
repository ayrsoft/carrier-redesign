import $ from 'jquery';
window.$ = window.jQuery = $;
import 'slick-carousel';

export class slider {
    constructor() {
        this.featureSlider = null;
        this.installationSlider = null;
        this.reviewsSlider = null;
    }

    getElements() {
        this.featureSlider = $('#features-slider');
        this.installationSlider = $('#installation-slider');
        
    }

    bindElements() {
        let _self = this;
        if (_self.featureSlider != null) {
            _self.featureSlider.slick({
                infinite: false,
                speed: 500,
                fade: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                cssEase: 'linear'
            });
        }
        if (_self.installationSlider != null) {
            _self.installationSlider.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
                variableWidth: true,
                responsive: [
                  {
                    breakpoint: 769,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      infinite: false,
                      variableWidth: false
                    }
                  },
                ]
              });
        }
    }

    init() {
        this.getElements();
        this.bindElements();

    }
}