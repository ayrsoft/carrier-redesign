export class header {
    constructor() {
        this.documentBody = null;
        this.menuDropdownTrigger = null;
        this.modalBackdrop = null;
        this.headerBackdrop = null;
        this.dataMenuHover = null;
        this.dataTriggerModal = null;
        this.dataModalClose = null;
        this.dataModalCloseFilter = null;
        this.showModalTrigger = null;
        this.bodyTrans = null;
    }

    getElements() {
        this.documentBody = document.querySelector('body');
        this.menuDropdownTrigger = document.querySelectorAll('[data-trigger-dropdown]');
        this.modalBackdrop = document.querySelector('[data-target="backdrop"]');
        this.headerBackdrop = document.querySelector('[data-target="headerbackdrop"]');
        this.dataMenuHover = document.querySelectorAll('[data-hover-dropdown]');
        this.dataTriggerModal = document.querySelectorAll('[data-trigger-modal]');
        this.dataModalClose = document.querySelectorAll('[data-modal-close="dismiss"]');
        this.showModalTrigger = document.querySelectorAll('[data-trigger-show]');
        this.dataModalCloseFilter = document.querySelector('[data-modal-close="filter"]');
        this.bodyTrans = document.querySelector('body');

    }

    bindElements() {
        let _self = this;

        window.addEventListener('load', function() {
            _self.bodyTrans.classList.remove('preload');
        });

        if (_self.menuDropdownTrigger != null) {
            _self.menuDropdownTrigger.forEach( (elem) => {
                elem.addEventListener('click', function () {
                    if (elem.parentNode.classList.contains('active')) {
                        elem.parentNode.classList.remove('active');
                        _self.activateModal(false);
                    } else {
                        _self.deactivateModals();
                        elem.parentNode.classList.add('active');
                        _self.activateModal(true);
                    }
                });
            });
        }
        
        if (_self.dataMenuHover != null) {
            _self.dataMenuHover.forEach( (elem) => {
                elem.addEventListener('mouseover', function () {
                    _self.deactivateModals();
                    elem.classList.add('active');
                    _self.activateModal(true);
                });
                elem.addEventListener('mouseleave', function () {
                    _self.deactivateModals();
                    elem.classList.remove('active');
                    _self.activateModal(false);
                });
            });
        }

        if (_self.dataTriggerModal != null) {
            _self.dataTriggerModal.forEach((elem) => {
                elem.addEventListener('click', function () {
                    let targetModal = this.getAttribute('data-trigger-modal');
                    let modalContainer = document.querySelector(`[data-modal-holder="${targetModal}"]`);
                    modalContainer.classList.add('active');
                    _self.activateModal(true, 'over');
                });
            });
        }

        if (_self.dataModalClose != null) {
            _self.dataModalClose.forEach((elem) => {
                elem.addEventListener('click', function () {
                    if (this.parentNode.getAttribute('data-modal-holder') == 'cart') {
                        this.parentNode.classList.remove('active');
                    } else {
                        this.parentNode.parentNode.classList.remove('active');
                    }
                    _self.activateModal(false, 'over');
                });
            });
        }

        if (_self.dataModalCloseFilter != null) {
            _self.dataModalCloseFilter.addEventListener('click', function () {
                this.parentNode.parentNode.classList.remove('show');
                _self.activateModal(false, 'over');
            });
        }

        if (_self.showModalTrigger != null && _self.showModalTrigger.length) {
            _self.showModalTrigger.forEach(element => {
                element.addEventListener('click', function () {
                    const modalTarget = this.getAttribute('data-trigger-show');
                    console.log(modalTarget);
                    let modalContainer = document.querySelector(`[data-show="${modalTarget}"]`);
                    _self.activateModal(true, 'over');
                    modalContainer.classList.add('show');
                });
            })
        }

    }

    activateModal(status, over = 'main') {
        let _self = this;
        if (status) {
            _self.modalBackdrop.classList.add('active', over);
            _self.headerBackdrop.classList.add('active', over);
            _self.documentBody.classList.add('opened');

        } else {
            setTimeout(function () {
                _self.headerBackdrop.classList.remove('active', over);
            }, 500);
            _self.modalBackdrop.classList.remove('active', over);
            _self.documentBody.classList.remove('opened');
        }
    }

    deactivateModals() {
        let _self = this;
        _self.menuDropdownTrigger.forEach( (elemMain) => {
            elemMain.parentNode.classList.remove('active');
        });
    }

    init() {
        this.getElements();
        this.bindElements();
    }
}