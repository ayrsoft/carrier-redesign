<?php include 'src/includes/header.php';?>
<?php include 'src/includes/breadcrumbs.php';?>
<section class="my-account profile">
        <div class="flex-content ">
            <?php include 'src/includes/my-account-sidebar.php';?>
            <div class="edit-profile">
                <h2 class="title">Edit Profile</h2>
                <div>
                    <form action="">
                        <label for="displaName">DISPLAY NAME</label><br>
                        <input type="text" id="displaName" name="displaName" placeholder="Stella M." value=""><br>
                        <span>Your display name will be used in reviews.</span>
                        <label for="fName">FIRST NAME</label><br>
                        <input type="text" id="fName" name="fName" placeholder="First Name" value=""><br>
                        <label for="lName">LAST NAME</label><br>
                        <input type="text" id="lName" name="lName" placeholder="Last  Name" value=""><br>
                        <label for="email">EMAIL</label><br>
                        <input type="text" id="email" name="email" placeholder="Last  Name" value=""><br>
                        <label for="phoneNumber">PHONE NUMBER</label><br>
                        <input type="text" id="phoneNumber" name="phoneNumber" placeholder="+63" value=""><input type="text" id="phoneNumber" name="phoneNumber" placeholder="555-555-5555" value=""><br>
                        <label for="password">PASSWORD</label> <span>Edit</span><br>
                        <input type="password" id="password" name="password" placeholder="●●●●●●●●●●●●" value=""><br>
                        <input type="checkbox" id="editProfile" name="editProfile">
                        <span>I'd like to subscribe to your e-newsletter to receive news and promos from Carrier.</span><br>
                        <button class="o-button-full">Save Changes</button>
                    </form> 
                </div>
            </div>
            <?php include 'src/includes/my-account-info-card.php';?>
        </div>
</section>
<div class="footer-border-top"></div>
<?php include 'src/includes/footer.php';?>