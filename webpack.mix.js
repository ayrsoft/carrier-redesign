let mix = require('laravel-mix');

mix.js('src/js/main.js', 'dist/')
   .sourceMaps()
   .sass('src/scss/main.scss', 'dist/')
   .copy('src/images', 'dist/images')
   .setPublicPath('dist');

